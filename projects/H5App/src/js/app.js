angular.module('H5App', [
    'ngRoute',
    'mobile-angular-ui',
    'ngAnimate',
    'toastr',
    'yaea.auth',
    'H5App.directives.Product',
    'H5App.controllers.Main',
    'H5App.controllers.Home',
    'H5App.controllers.GoodsDetails',
    'H5App.controllers.Kinds',
    'H5App.controllers.Evaluation',
    'H5App.directives.Productdetail',
    'H5App.directives.Wechat',
    'H5App.controllers.PersonalCenter',
    'H5App.controllers.WaitforPay',
    'H5App.controllers.WaitforDeliver',
    'H5App.controllers.WaitforTakedelivery',
    'H5App.controllers.WaitforEvaluate',
    'H5App.controllers.TakedeliveryAddress',
    'H5App.controllers.EditAddress',
    'H5App.controllers.AllOrders',
    'H5App.controllers.ExpressEvaluation',
    'H5App.controllers.ShoppingCard',
    'H5App.controllers.AccountManage',
    'H5App.controllers.ModifyPersonalinfo',
    'H5App.controllers.ModifyPassword',
    'H5App.controllers.ModifyPhoneNum',
    'H5App.controllers.FillinTheorder',
    'H5App.controllers.FindbackPassword',
    'H5App.controllers.Register',
    'H5App.controllers.Login',
    'H5App.controllers.Payment',
    'H5App.services.Page',
    'H5App.services.ShoppingCart',
    'H5App.services.Product',
    'H5App.services.Recipient',
    'H5App.controllers.ProductList',
    'H5App.controllers.ReplyEvaluation',
    'H5App.controllers.FinishPay',
    'H5App.controllers.OrderDetail',
    'H5App.services.Order'
])

    .config([
        '$routeProvider',
        '$locationProvider',
        'toastrConfig',
        function ($routeProvider, $locationProvider, toastrConfig) {
            $routeProvider.when('/',
                {
                    controller: 'HomeController',
                    controllerAs: 'HomeCtl',
                    resolve: {
                        layoutData: function (PageService) {
                            return PageService.getLayout('Main');
                        }
                    },
                    templateUrl: 'home.html'
                })
                .when('/kinds', { controller: 'KindsController', templateUrl: 'kinds.html' })
                .when('/goodsdetails/:productid',
                {
                    controller: 'GoodsDetailsController',
                    controllerAs: 'DetailCtl',
                    resolve: {
                        initData: function ($route, ProductService) {
                            return ProductService.getProductDetail($route.current.params.productid);
                        },
                        productData: function ($route, ProductService) {
                            return ProductService.getProductInfo($route.current.params.productid);
                        }
                    },
                    templateUrl: 'goodsdetails.html'
                })
                .when('/evaluation', { controller: 'EvaluationController', templateUrl: 'evaluation.html' })
                .when('/personalcenter',
                {
                    authenticate: true,
                    controller: 'PersonalCenterController',
                    templateUrl: 'personalcenter.html'
                })
                .when('/waitforpay', { controller: 'WaitforPayController', templateUrl: 'waitforpay.html' })
                .when('/waitfordeliver', { controller: 'WaitforDeliverController', templateUrl: 'waitfordeliver.html' })
                .when('/waitfortakedelivery', { controller: 'WaitforTakedeliveryController', templateUrl: 'waitfortakedelivery.html' })
                .when('/waitforevaluate', { controller: 'WaitforEvaluateController', templateUrl: 'waitforevaluate.html' })
                .when('/takedeliveryaddress',
                {
                    authenticate: true,
                    controller: 'TakedeliveryAddressController',
                    templateUrl: 'takedeliveryaddress.html',
                    resolve: {
                        initData: function ($route, RecipientService) {
                            return RecipientService.getRecipient();
                        }
                    },
                })
                .when('/editaddress/:type/:id',
                {
                    authenticate: true,
                    controller: 'EditAddressController',
                    templateUrl: 'editaddress.html'
                })
                .when('/allorders', { controller: 'AllOdersController', templateUrl: 'allorders.html' })
                .when('/expressevaluation', { controller: 'ExpressEvaluationController', templateUrl: 'expressevaluation.html' })
                .when('/shoppingcard',
                {
                    controller: 'ShoppingCardController',
                    templateUrl: 'shoppingcard.html'
                })
                .when('/accountmanage', { controller: 'AccountManageController', templateUrl: 'accountmanage.html' })
                .when('/modifypersonalinfo', { controller: 'ModifyPersonalinfoController', templateUrl: 'modifypersonalinfo.html' })
                .when('/modifypassword', { controller: 'ModifyPasswordController', templateUrl: 'modifypassword.html' })
                .when('/modifyphonenumstepone', { controller: 'ModifyPhoneNumController', templateUrl: 'modifyphonenumstepone.html' })
                .when('/modifyphonenumsteptwo', { controller: 'ModifyPhoneNumController', templateUrl: 'modifyphonenumsteptwo.html' })
                .when('/modifyphonenumstepthree', { controller: 'ModifyPhoneNumController', templateUrl: 'modifyphonenumstepthree.html' })
                .when('/fillintheorder',
                {
                    authenticate: true,
                    controller: 'FillinTheorderController',
                    templateUrl: 'fillintheorder.html'
                })
                .when('/findbackpassword', { controller: 'FindbackPasswordController', templateUrl: 'findbackpassword.html' })
                .when('/register', { controller: 'RegisterController', templateUrl: 'register.html' })
                .when('/login', { controller: 'LoginController', templateUrl: 'login.html' })
                .when('/productlist/:relation',
                {

                    controller: 'ProductListController',
                    controllerAs: 'ctl',
                    resolve: {
                        initData: function ($route, ProductService) {
                            return ProductService.getProductDetail($route.current.params.relation);
                        }
                    },
                    templateUrl: 'productlist.html'

                })
                .when('/replyevaluation', { controller: 'ReplyEvaluationController', templateUrl: 'replyevaluation.html' })
                .when('/payment', {
                    authenticate: true,
                    controller: 'PaymentController',
                    templateUrl: 'payment.html'
                })
                .when('/finishpay', {
                    authenticate: true,
                    controller: 'FinishPayController',
                    templateUrl: 'finishpay.html'
                })
                .when('/orderdetail/:id', {
                    authenticate: true,
                    templateUrl: 'orderdetail.html',
                    controller: 'OrderDetailController',
                    resolve: {
                        initData: function ($route, OrderService) {
                            return OrderService.getOrder($route.current.params.id);
                        }
                    },
                });

            $locationProvider.html5Mode(true);
            angular.extend(toastrConfig, {
                autoDismiss: false,
                positionClass: 'toast-bottom-center',
                timeout: '500',
                extendedTimeout: '200',
                tapToDismiss: true,
                progressBar: false,
                closeHtml: '<button>&times;</button>',
                newestOnTop: true,
                maxOpened: 0,
                preventDuplicates: false,
                preventOpenDuplicates: false
            });
        }]);
