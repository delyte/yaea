angular.module('H5App.controllers.Dialog', [])
    .controller('DialogController', [
        '$scope', '$uibModalInstance', 'dataInstance',
        function ($scope, $modalInstance, dataInstance) {
            var self = this;

            $scope.title = dataInstance.title;
            $scope.content = dataInstance.content;
            self.ok = function () {
                $modalInstance.close(true);
            };

            self.cancel = function () {
                $modalInstance.dismiss(false);
            };


        }]);