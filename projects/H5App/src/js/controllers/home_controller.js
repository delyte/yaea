angular.module('H5App.controllers.Home', ['H5App.services.Product'])

    .controller('HomeController',
    ['$scope', '$location', 'ProductService', 'layoutData', 'LocationService',
        function ($scope, $location, ProductService, layoutData, LocationService) {
            var TurntoUrl = function (url) {
                LocationService.path(url);
            };

            $scope.layoutData = layoutData;

            this.clickSlide = function (productId) {
                TurntoUrl('/goodsdetails/' + productId)
            }

            var self = this;

            this.itemClick = function (item) {
                if (self.currentItem.isMore && self.currentItem.isMore == true) {
                    TurntoUrl('/productlist/' + self.currentItem.relation);
                }
                else {
                    TurntoUrl('/goodsdetails/' + self.currentItem.id);
                }
            };

            this.showCategoriesProduct = function () {
                TurntoUrl('/productlist');

            };

        }]);