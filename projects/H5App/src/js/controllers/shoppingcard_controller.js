angular.module('H5App.controllers.ShoppingCard', ['H5App.services.Dialog'])

    .controller('ShoppingCardController',
    ['$scope', '$location', 'ShoppingCartService', 'DialogService', 'RecipientService', 'OrderService', 'LocationService',
        function ($scope, $location, ShoppingCartService, DialogService, RecipientService, OrderService, LocationService) {

            $scope.kindcount = 0;

            $scope.total = 0;

            $scope.cart = ShoppingCartService.getCartFromLocal().cart || [];

            $scope.$watch('cart', function (newValue, oldValue) {
                var cartinfo = ShoppingCartService.getCartFromLocal();
                cartinfo.cart = newValue;
                ShoppingCartService.saveCartIntoLocal(cartinfo)
            }, true);

            $scope.add = function (item) {
                item.count++;
                computeTotal();
            }

            $scope.reduce = function (item) {
                if (item.count > 1) {
                    item.count--;
                }
                computeTotal();
            }

            function computeTotal() {
                var result = _.filter($scope.cart, function (o) { return o.checked; });
                $scope.kindcount = result.length;
                $scope.total = 0;
                angular.forEach(result, function (item) {
                    $scope.total += item.count * item.product.price;
                })
            }

            $scope.checkitem = function (value) {
                computeTotal();
            }

            $scope.remove = function (index) {
                DialogService.showConfirmDialog('删除', '确定删除该商品？').then(function (yes) {
                    if (yes) {
                        _.pullAt($scope.cart, index);
                        computeTotal();
                    }
                })

            }

            $scope.$on('$destroy', function iVeBeenDismissed() {
                ShoppingCartService.savelocalCarttoSever();
            });



            function setOrder(suceess) {
                return function (data) {
                    var result = _.filter($scope.cart, function (o) { return o.checked; });
                    _.remove($scope.cart, function (n) {
                        return n.checked;
                    })
                    var item = angular.copy($scope.buyInfo);
                    var order = {
                        status: 'Obligation',
                        orderedOn: Date,
                        recipient: suceess ? data : null,
                        orders: result,
                        bill: {
                            productBill: $scope.total
                        }
                    }
                    OrderService.setCurrentOrder(order)
                    LocationService.path('/fillintheorder');
                }
            }


            $scope.submitOrder = function () {
                RecipientService.getDefaulted().then(setOrder(true)).catch(setOrder(false))
            }

            computeTotal();
        }]);