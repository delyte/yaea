angular.module('H5App.controllers.Register', [
    'ManagerApp.services.User'
])

    .controller('RegisterController',
    [
        '$scope',
        '$location',
        '$interval',
        'UserService',
        'toastr',
        'Auth',
        'LocationService',
        function ($scope, $location, $interval, UserService, toastr, Auth, LocationService) {
            $scope.user = {
                name: '',
                phonenumber: '',
                validCode: '',
                password: ''
            };

            $scope.repassword = '';

            var phonenumberRegex = /(^(13\d|14[57]|15[^4,\D]|17[13678]|18\d)\d{8}|170[^346,\D]\d{7})$/;

            $scope.sendflag = true;
            $scope.getauthcodebtntext = '获取验证码';

            $scope.getauthcodebtnbackcolor = '#F3AF34';

            $scope.warningMsg = '';

            $scope.sendAuthCode = function () {
                if (!phonenumberRegex.test($scope.user.phonenumber)) {
                    toastr.error('请正确填写手机号码', '注意');
                    return false;
                }
                UserService.sendValidCode($scope.user.phonenumber).then(function (data) {
                    $scope.sendflag = false;
                    var handleSeconds = 120;
                    $scope.getauthcodebtnbackcolor = '#CECDCB';
                    $scope.getauthcodebtntext = '重新发送(' + handleSeconds + ')';
                    toastr.info('短信已经发送到您的手机，如在120秒之内还没有收到短信验证吗，请重新获取验证码。', '提醒');
                    $interval(function () {
                        handleSeconds--;
                        $scope.getauthcodebtntext = '重新发送(' + handleSeconds + ')';
                    }, 1000, 120).then(function () {
                        $scope.sendflag = true;
                        $scope.getauthcodebtnbackcolor = '#F3AF34';
                        $scope.getauthcodebtntext = '获取验证码';
                    });

                }).catch(function (err) {
                    toastr.error(err.data.msg || '请规范填写', '注意');
                    $scope.warningMsg = err.data.msg;
                });

            };

            $scope.doRegister = function () {
                if (dovalid()) {
                    Auth.createUser($scope.user).then(function (data) {
                        LocationService.goBack();
                    }).catch(function (err) {
                        toastr.error(err.data.errors.name.message || '注册报错', '注意');
                    })
                }
            }

            function dovalid() {
                if (angular.isUndefined($scope.user.name) || $scope.user.name == null || $scope.user.name.trim() == '') {
                    toastr.error('用户名不能为空', '注意');
                    return false;
                }
                if (angular.isUndefined($scope.user.phonenumber) || $scope.user.phonenumber == null || $scope.user.phonenumber.trim() == '') {
                    toastr.error('手机号不能为空', '注意');
                    return false;
                }
                if (!phonenumberRegex.test($scope.user.phonenumber)) {
                    toastr.error('请正确填写手机号码', '注意');
                    return false;
                }
                if (angular.isUndefined($scope.user.validCode) || $scope.user.validCode == null || $scope.user.validCode.trim() == '') {
                    toastr.error('验证码不能为空', '注意');
                    return false;
                }
                if (angular.isUndefined($scope.user.password) || $scope.user.password == null || $scope.user.password.trim() == '') {
                    toastr.error('密码不能为空', '注意');
                    return false;
                }
                if ($scope.user.password != $scope.repassword) {
                    toastr.error('设置的密码不一致', '注意');
                    return false;
                }
                return true;

            }



        }]);