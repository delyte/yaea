angular.module('H5App.controllers.FinishPay', [])

    .controller('FinishPayController', ['$scope', 'UtilityService', 'WeChatService', 'OrderService', 'LocationService',
        function ($scope, UtilityService, WeChatService, OrderService, LocationService) {
            var currentOrder = OrderService.getCurrentOrder();
            $scope.bill = currentOrder.bill.productBill;
            $scope.sn = currentOrder.sn;
            $scope.viewOrder = function () {
                LocationService.path('/orderdetail/' + currentOrder._id);
            }
        }]);