angular.module('H5App.controllers.Main', ['ui.bootstrap'])

    .controller('MainController', ['$scope', 'Auth', 'LocationService', 'UtilityService', 'WeChatService',
        function ($scope, Auth, LocationService, UtilityService, WeChatService) {

            WeChatService.initAuth();
            $scope.goBack = function () {
                LocationService.goBack();
            };

            $scope.TurntoUrl = function (url) {
                LocationService.path(url);
            };

            $scope.goHome = function (url) {
                LocationService.goHome();
            };

            $scope.$on("$routeChangeStart", function (event, next, current) {
                var nextRout = next.$$route;
                if (!nextRout.authenticate) {
                    return;
                }

                if (typeof nextRout.authenticate === 'string') {
                    Auth.hasRole(nextRout.authenticate, angular.noop).then(function (has) {
                        if (has) {
                            return;
                        }

                        event.preventDefault();
                        return Auth.isLoggedIn(angular.noop).then(function (is) {
                            LocationService.path(is ? '/' : '/login');
                        });
                    });
                } else {
                    Auth.isLoggedIn(angular.noop).then(function (is) {
                        if (is) {
                            return;
                        }

                        event.preventDefault();
                        LocationService.path('/login');
                    });
                }



            });
        }]);