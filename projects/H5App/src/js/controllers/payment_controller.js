angular.module('H5App.controllers.Payment', [])

    .controller('PaymentController', ['$scope', 'UtilityService', 'WeChatService', 'OrderService', 'LocationService',
        function ($scope, UtilityService, WeChatService, OrderService, LocationService) {
            var currentOrder = OrderService.getCurrentOrder();
            $scope.bill = currentOrder.bill.productBill;
            $scope.pay = function () {
                if (currentOrder && currentOrder.sn) {
                    var content = currentOrder.orders.length > 1 ? currentOrder.orders[0].product.name + '等等' : currentOrder.orders[0].product.name;
                    UtilityService.getWxPayconfig(content, currentOrder.sn, $scope.bill * 100).then(function (config) {

                        WeChatService.chooseWXPay({
                            timestamp: config.data.timeStamp,
                            nonceStr: config.data.nonceStr,
                            package: config.data.package,
                            signType: config.data.signType,
                            paySign: config.data.paySign,
                            success: function (res) {
                                OrderService.nextStatus().then(function (doc) {
                                    LocationService.pathWithClear('/finishpay');
                                })
                            }
                        });
                    })
                }
            }
        }]);