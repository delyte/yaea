angular.module('H5App.controllers.Login', [])

    .controller('LoginController',
    ['$scope', 'LocationService', 'toastr', 'Auth', 'ShoppingCartService',
        function ($scope, LocationService, toastr, Auth, ShoppingCartService) {
            $scope.user = {
                phonenumber: '',
                password: ''
            };
            $scope.checkAccountAndLogin = function () {
                if (dovalid()) {
                    Auth.login($scope.user).then(function (data) {
                        ShoppingCartService.syncShoppingCartAfterLogin();
                        LocationService.goBack();
                    }).catch(function (err) {
                        toastr.info('登陆失败，请检查您的用户名和密码', '提醒');
                    });
                }
            };

            function dovalid() {
                if (angular.isUndefined($scope.user.phonenumber) || $scope.user.phonenumber == null || $scope.user.phonenumber.trim() == '') {
                    toastr.error('用户名不能为空', '注意');
                    return false;
                }
                if (angular.isUndefined($scope.user.password) || $scope.user.password == null || $scope.user.password.trim() == '') {
                    toastr.error('密码不能为空', '注意');
                    return false;
                }

                return true;

            }


        }]);