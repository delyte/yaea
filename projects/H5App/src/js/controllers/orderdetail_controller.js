angular.module('H5App.controllers.OrderDetail', [])

    .controller('OrderDetailController', ['$scope', 'UtilityService', 'WeChatService', 'OrderService', 'LocationService', 'initData',
        function ($scope, UtilityService, WeChatService, OrderService, LocationService, initData) {
            $scope.order = initData;
        }]);