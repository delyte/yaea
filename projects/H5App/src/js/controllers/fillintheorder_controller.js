angular.module('H5App.controllers.FillinTheorder', [])

    .controller('FillinTheorderController', ['$scope', 'LocationService', 'OrderService', function ($scope, LocationService, OrderService) {

        $scope.order = OrderService.getCurrentOrder();

        $scope.submitOrder = function () {
            LocationService.path('/payment');
        }
    }]);