angular.module('H5App.controllers.TakedeliveryAddress', [])

    .controller('TakedeliveryAddressController', [
        '$scope', 'initData', '$location', 'RecipientService', 'DialogService', 'OrderService', 'LocationService',
        function ($scope, initData, $location, RecipientService, DialogService, OrderService, LocationService) {

            $scope.addresses = initData;

            $scope.edit = function (id) {
                LocationService.path('/editaddress/edit/' + id);
            }

            $scope.selectRecip = function (recip) {
                OrderService.setRecipientForOrder(recip);
                LocationService.goBack();
            }

            $scope.remove = function (index, id) {
                DialogService.showConfirmDialog('删除', '确定删除该地址？').then(function (yes) {
                    if (yes) {
                        RecipientService.deleteRecipient(id).then(function () {
                            _.pullAt($scope.addresses, index);
                        })
                    }
                })

            }

        }]);