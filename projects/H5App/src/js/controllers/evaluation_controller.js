angular.module('H5App.controllers.Evaluation', [])

    .controller('EvaluationController', function ($scope,$location) {
        
        $scope.evaluations = [
            {
                productId: 1,
                id: 1,
                phoneNo: '13905665244',
                date: '2016-02-23',
                overStar: '5',
                description: '挺划算的。'
            },
            {
                productId: 1,
                id: 2,
                phoneNo: '13905665255',
                date: '2016-02-23',
                overStar: '5',
                description: '好吃'
            },
            {
                productId: 1,
                id: 3,
                phoneNo: '13905665233',
                date: '2016-02-23',
                overStar: '4',
                description: '很甜'
            }
        ];

        $scope.rate = 5;
        $scope.max = 5;
        $scope.isReadonly = false;




    });