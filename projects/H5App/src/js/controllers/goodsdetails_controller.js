angular.module('H5App.controllers.GoodsDetails', ['H5App.services.ShoppingCart', 'H5App.services.Order', 'H5App.services.Recipient'])

    .controller('GoodsDetailsController', [
        '$scope',
        '$location',
        'productData',
        'ShoppingCartService',
        'OrderService',
        'RecipientService',
        'LocationService',
        'Auth',
        function ($scope, $location, productData, ShoppingCartService, OrderService, RecipientService, LocationService, Auth) {



            $scope.productDetailModel = productData;
            $scope.cartcount = 0;

            var carts = ShoppingCartService.getCartFromLocal().cart || [];

            angular.forEach(carts, function (cart) {
                $scope.cartcount += cart.count;
            })


            this.addInCart = function () {
                var item = angular.copy($scope.buyInfo);
                ShoppingCartService.addCart(item);
                $scope.cartcount += item.count;
            }

            this.purchase = function (url) {
                Auth.isLoggedIn(angular.noop).then(function (is) {
                    if (is) {
                        RecipientService.getDefaulted().then(setOrder(true)).catch(setOrder(false))
                    } else {
                        LocationService.path('/login');
                    }
                });

            };

            function setOrder(suceess) {
                return function (data) {
                    var item = angular.copy($scope.buyInfo);
                    var order = {
                        status: 'Obligation',
                        orderedOn: Date,
                        recipient: suceess ? data : null,
                        orders: [item],
                        bill: {
                            productBill: item.count * item.product.price
                        }
                    }
                    OrderService.setCurrentOrder(order)
                    LocationService.path('/fillintheorder');
                }
            }

            this.TurntoUrl = function (url) {
                LocationService.path(url);
            };

        }]);