angular.module('H5App.controllers.EditAddress', ['yaea.common.amap'])

    .controller('EditAddressController', ['$scope', 'AmapService', 'RecipientService', 'LocationService', '$routeParams',
        function ($scope, AmapService, RecipientService, LocationService, $routeParams) {

            var type = $routeParams.type,
                id = $routeParams.id;

            $scope.recipient = {
                name: '',
                phone: '',
                address: '',
                post: '',
                defaulted: false
            }

            if (type == 'edit') {
                RecipientService.getRecipientById($routeParams.id).then(function (data) {
                    $scope.recipient = data;

                    AmapService.getDistrict().then(function (dt) {
                        $scope.province = dt;
                        $scope.address = $scope.recipient.address;
                        if ($scope.recipient.post && $scope.recipient.post != '') {
                            var provincepost = $scope.recipient.post.substr(0, 3),
                                provincepostRex = new RegExp('^' + provincepost);
                            var citypost = $scope.recipient.post.substr(0, 4),
                                citypostRex = new RegExp('^' + citypost);
                            $scope.selectprovince = _.find($scope.province, function (o) { return provincepostRex.test(o.adcode) });
                            if ($scope.selectprovince) {
                                $scope.address = $scope.address.substr($scope.selectprovince.name.length);
                                $scope.selectcity = _.find($scope.selectprovince.districts, function (o) { return citypostRex.test(o.adcode) });
                                if ($scope.selectcity) {
                                    $scope.address = $scope.address.substr($scope.selectcity.name.length);
                                    $scope.selectdistrict = _.find($scope.selectcity.districts, function (o) { return $scope.recipient.post == o.adcode });
                                    if ($scope.selectdistrict) {
                                        $scope.address = $scope.address.substr($scope.selectdistrict.name.length);
                                    }
                                }
                            }
                        }

                    });
                });
            }
            else {
                AmapService.getDistrict().then(function (data) {
                    $scope.province = data;
                });
            }






            $scope.$watch('selectprovince', function (data) {
                $scope.city = data ? data.districts : null;
            });

            $scope.$watch('selectcity', function (data) {
                $scope.district = data ? data.districts : null;
            });


            $scope.save = function () {
                $scope.recipient.address = ($scope.selectprovince ? $scope.selectprovince.name : '')
                    + ($scope.selectcity ? $scope.selectcity.name : '')
                    + ($scope.selectdistrict ? $scope.selectdistrict.name : '') + $scope.address;
                $scope.recipient.post = $scope.selectdistrict ? $scope.selectdistrict.adcode : '';
                if (type == 'new') {
                    RecipientService.createRecipient($scope.recipient).then(function (data) {
                        LocationService.goBack();
                    })
                } else {
                    RecipientService.updateRecipient(id, $scope.recipient).then(function (data) {
                        $location.goBack();
                    })
                }
            }


        }]);