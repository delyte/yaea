angular.module('H5App.directives.Wechat', ['H5App.services.Utility', 'App.services.WeChat'])
    .constant('YaeaInjectConfig', {
        jsUrl: 'http://res.wx.qq.com/open/js/jweixin-1.0.0.js'
    })
    .controller('YaeaInjectController', ['$scope', '$attrs', '$window', 'UtilityService', 'WeChatService', function ($scope, $attrs, $window, UtilityService, WeChatService) {
        $window.onload = function () {
            if (wx) {
                UtilityService.getWxSDKconfig().then(
                    function (data) {
                        wx.config(data.data);
                        wx.ready(function () {
                            WeChatService.initionaled = true;
                        });
                    }
                );
            }
        }
    }])
    .directive('yaeaInject', ['$window', '$q', 'YaeaInjectConfig', function ($window, $q, YaeaInjectConfig) {
        function load_script() {
            var s = document.createElement('script');
            s.src = YaeaInjectConfig.jsUrl;
            document.head.appendChild(s);
        }
        return {
            restrict: 'EA',
            controller: 'YaeaInjectController',
            link: function (scope, element, attrs) {
                if ($window.wx) {
                    console.log('wechatSDK already loaded');
                } else {
                    load_script();
                }
            }
        };
    }]);