angular.module('H5App.directives.Productdetail', [])
    .controller('YaeaProductdetailController', ['$scope', '$attrs','$location', function($scope, $attrs,$location) {
        var scrollableContentCtrl = { scrollTo: angular.noop },
            ngModelCtrl = { $setViewValue: angular.noop },
            self = this;
        $scope.buyinfo = {
            product: $scope.productdetailModel,
            count: 1
        };

        self.init = function(scrollableContentCtrl_, ngModelCtrl_) {
            scrollableContentCtrl = scrollableContentCtrl_,
                ngModelCtrl = ngModelCtrl_;
            ngModelCtrl.$setViewValue($scope.buyinfo);
            ngModelCtrl.$render = self.render;
        };
        self.reduce = function() {
            if ($scope.buyinfo.count > 0) {
                $scope.buyinfo.count--;
            }
            ngModelCtrl.$setViewValue($scope.buyinfo);
            ngModelCtrl.$render();
        };
        self.add = function() {
            if ($scope.buyinfo.count < $scope.productdetailModel.inStock) {
                $scope.buyinfo.count++;
            }
            ngModelCtrl.$setViewValue($scope.buyinfo);
            ngModelCtrl.$render();
        };

        self.render = function() {
            $scope.buyinfo = ngModelCtrl.$viewValue;
        };



        self.scrollToDetailPics = function() {
            var elem = angular.element(document.getElementById('pageDetailsDiv'));
            scrollableContentCtrl.scrollTo(elem);
        };
        
        self.TurntoUrl = function (url) {
            $location.path(url);
        };

    }])
    .directive('yaeaProductdetail', function() {
        return {
            scope: { productdetailModel: '=' },
            require: ['yaeaProductdetail', '?^scrollableContent', 'ngModel'],
            restrict: 'AE',
            replace: true,
            controller: 'YaeaProductdetailController',
            templateUrl: 'productdetailTemplate_directive.html',
            controllerAs: 'detailCtrl',
            link: function(scope, element, attrs, ctrls) {
                var YaeaProductdetailCtrl = ctrls[0], scrollableContentCtrl = ctrls[1], ngModelCtrl = ctrls[2];
                YaeaProductdetailCtrl.init(scrollableContentCtrl, ngModelCtrl);
            }
        }
    });