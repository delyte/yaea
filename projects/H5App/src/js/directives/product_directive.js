angular.module('H5App.directives.Product', [])

    .controller('YaeaProductController', ['$scope', '$attrs', function ($scope, $attrs) {

        var ngModelCtrl = { $setViewValue: angular.noop },
            self = this;
        self.init = function (ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_;
            ngModelCtrl.$setViewValue($scope.selectedItem);
            ngModelCtrl.$render = self.render
        };
        self.itemClick = function (item) {
            if (angular.isDefined($scope.productItemClick)) {

                $scope.selectedItem = item;
                ngModelCtrl.$setViewValue($scope.selectedItem);
                ngModelCtrl.$render();
                $scope.productItemClick();
            }
        };

        self.moreItemClick = function (params) {
            var moreItem = {
                isMore: true,
                relation: $scope.productModel.productRelation
            };
            ngModelCtrl.$setViewValue(moreItem);
            ngModelCtrl.$render();
            $scope.productItemClick();
        };

        self.render = function () {
            $scope.selectedItem = ngModelCtrl.$viewValue;
        };
    }])
    .directive('yaeaProduct', function () {
        return {
            scope: { productModel: '=', productItemClick: '&' },
            restrict: 'AE',
            replace: true,
            templateUrl: 'productTemplate_directive.html',
            controller: 'YaeaProductController',
            controllerAs: 'productCtrl',
            require: ['yaeaProduct', 'ngModel'],
            link: function (scope, element, attrs, ctrls) {
                var YaeaProductCtrl = ctrls[0], ngModelCtrl = ctrls[1];
                YaeaProductCtrl.init(ngModelCtrl);
            }
        }
    });