'use strict';
angular.module('H5App.services.Dialog', ['H5App.controllers.Dialog'])
    .constant('DialogServiceConfig', {

        WarningDialog: {
            templateUrl: 'dialog_warning.html',
            controller: 'DialogController',
            controllerAs: 'dialogCtrl'
        },
        ConfirmDialog: {
            templateUrl: 'dialog_confirm.html',
            controller: 'DialogController',
            controllerAs: 'dialogCtrl'
        }
    })
    .service('DialogService', ['$uibModal', 'DialogServiceConfig', function ($modal, config) {


        this.showConfirmDialog = function (title, content) {
            var modalInstance = $modal.open({
                templateUrl: config.ConfirmDialog.templateUrl,
                controller: config.ConfirmDialog.controller,
                controllerAs: config.ConfirmDialog.controllerAs,
                size: 'sm',
                resolve: {
                    dataInstance: function () {
                        return {
                            title: title,
                            content: content
                        }
                    }
                },
            });
            return modalInstance.result;
        };

        this.showWarningDialog = function (title, content) {
            var modalInstance = $modal.open({
                templateUrl: config.WarningDialog.templateUrl,
                controller: config.WarningDialog.controller,
                controllerAs: config.WarningDialog.controllerAs,
                size: 'sm',
                resolve: {
                    dataInstance: function () {
                        return {
                            title: title,
                            content: content
                        }
                    }
                },
            });
            return modalInstance.result;
        };


    }]);