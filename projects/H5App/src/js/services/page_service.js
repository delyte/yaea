angular.module('H5App.services.Page', ['yaea.resource.page'])
    .service('PageService', ['$http', '$q', 'Page', function ($http, $q, Page) {
        this.getLayout = function (type) {
            return Page.layout({ type: type }).$promise;
        };
    }])
    .filter('firstProduct', function () {
        return function (productList) {
            return productList[0];
        }
    })
    .filter('cutProductWithoutFirst', function () {
        return function (productList, listRelation) {
            if (productList.length > 7) {
                var newlist = _.slice(productList, 1, 6)
                return newlist;
            }
            else {
                return _.slice(productList, 1, productList.length);
            }
        }
    });