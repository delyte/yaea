angular.module('H5App.services.Product', ['yaea.resource.product'])
    .constant('ProductServiceConfig', {
        productByCatoryUrl: '/data/getproductlist.json',
        productDetailUrl: '/data/productDetail.json'
    })

    .service('ProductService', ['$http', '$q', 'Product', 'ProductServiceConfig', function ($http, $q, Product, ProductServiceConfig) {
        this.getProductInfo = function (id) {
            return Product.get({ id: id }).$promise;
        };

        this.getProductByCatory = function () {
            var req = {
                method: 'GET',
                url: ProductServiceConfig.productByCatoryUrl
            };

            return $http(req)
        };

        this.getProductDetail = function (productId) {
            var req = {
                method: 'GET',
                url: ProductServiceConfig.productDetailUrl,
                params: {
                    productid: productId
                }
            };

            return $http(req)
        }

    }]);
