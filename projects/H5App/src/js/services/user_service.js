angular.module('ManagerApp.services.User', ['yaea.resource.user'])
    .service('UserService', ['$http', '$q', 'User', function ($http, $q, User) {
        this.sendValidCode = function (phoneNumber) {
            return User.sendValidCode({ phoneNumber: phoneNumber }).$promise;
        };
    }]);