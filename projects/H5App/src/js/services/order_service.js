angular.module('H5App.services.Order', ['yaea.resource.order'])
    .service('OrderService', ['$http', '$q', 'Order', function ($http, $q, Order) {
        var currentOrder = {};

        var me = this;

        function convertOrder(data) {
            return $q(function (resolve, reject) {
                if (!angular.isString(data.recipient) && angular.isObject(data.recipient)) {
                    data.recipient = data.recipient._id
                }
                angular.forEach(data.orders, function (order) {
                    if (!angular.isString(order.product) && angular.isObject(order.product)) {
                        order.product = order.product._id;
                    }
                })
                resolve(data);
            });

        }



        this.nextStatus = function (id, order) {

            id = id || currentOrder._id,
                order = order || currentOrder;
            return $q(function (resolve, reject) {
                return Order.next({ id: id }, order).$promise.then(function (data) {
                    currentOrder = data;
                    resolve(true);
                }, reject)
            });

        };


        this.setCurrentOrder = function (order) {
            currentOrder = angular.copy(order);
            convertOrder(order).then(me.createOrder).then(function (data) {
                currentOrder = data;
            });
        }



        this.setRecipientForOrder = function (recipient) {
            currentOrder.recipient = recipient;
        }

        this.getCurrentOrder = function () {
            return currentOrder;
        }

        this.getOrderByStatus = function (status) {
            return Order.catgory({ status: status }).$promise;
        };

        this.getOrderCatgoryCount = function () {
            return Order.catgoryCount().$promise;
        };

        this.createOrder = function (order) {
            return Order.save(order).$promise;
        };

        this.updateOrder = function (id, order) {
            return Order.update({ id: id }, order).$promise;
        };

        this.deleteOrder = function (id) {
            return Order.delete({ id: id }).$promise;
        };

        this.getOrder = function (id) {
            return Order.get({ id: id }).$promise;
        };
    }]);