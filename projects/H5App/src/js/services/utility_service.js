angular.module('H5App.services.Utility', [])

    .service('UtilityService', ['$http', '$q', '$location', function ($http, $q, $location) {
        this.getWxSDKconfig = function (wx) {
            var pageUrl = window.location.origin + window.location.pathname;
            
            var req = {
                method: 'GET',
                url: '/api/weixin/jssdk',
                params: {
                    pageUrl: pageUrl
                }
            };

            return $http(req);
        };


        this.getWxPayconfig = function (description, orderNumber, totalFee) {
            var req = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },

                url: '/api/weixin/payconfig',
                data: {
                    description: description,
                    orderNumber: orderNumber,
                    totalFee: totalFee
                }
            };

            return $http(req);
        };
    }]);
