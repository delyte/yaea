angular.module('H5App.services.Recipient', ['yaea.resource.recipient'])
    .service('RecipientService', ['$http', '$q', 'Recipient', 'ProductServiceConfig', function ($http, $q, Recipient) {
        this.getRecipient = function () {
            return Recipient.query().$promise;
        };

        this.getRecipientById = function (id) {
            return Recipient.get({ id: id }).$promise;
        };

        this.createRecipient = function (recipient) {
            return Recipient.save(recipient).$promise;
        };
        
        this.getDefaulted = function () {
            return Recipient.defaulted().$promise;
        };

        this.updateRecipient = function (id, recipient) {
            return Recipient.update({ id: id }, recipient).$promise;
        };

        this.deleteRecipient = function (id) {
            return Recipient.delete({ id: id }).$promise;
        };
    }]);