'use strict';
angular.module('App.services.WeChat', [])
    .service('WeChatService', ['$http', '$location', '$q', 'UtilityService', function ($http, $location, $q, UtilityService) {

        var me = this;

        this.initionaled = false;

        this.initAuth = function () {
            return $q(function (resolve, reject) {
                var query = $location.search();
                if (query.code && query.state) {
                    var req = {
                        method: 'GET',
                        url: '/api/weixin/auth',
                        params: query
                    };
                    return $http(req).then(resolve, reject);
                } else {
                    resolve('不需要验证微信');
                }
            });
        }


        this.reConfig = function () {
            me.initionaled = false;
            return $q(function (resolve, reject) {
                UtilityService.getWxSDKconfig().then(
                    function (data) {
                        wx.config(data.data);
                        wx.ready(function () {
                            me.initionaled = true;
                            resolve();
                        });
                    }, reject
                );
            });
        }

        this.checkApi = function (opt) {
            if (!me.initionaled)
                return;
            wx.checkJsApi(opt);

        };


        this.chooseImage = function (opt) {
            if (!me.initionaled)
                return;
            wx.chooseImage(opt);

        };


        this.previewImage = function (opt) {
            if (!me.initionaled)
                return;
            wx.previewImage(opt);

        };

        this.uploadImage = function (opt) {
            if (!me.initionaled)
                return;
            wx.uploadImage(opt);

        };

        this.downloadImage = function (opt) {
            if (!me.initionaled)
                return;
            wx.downloadImage(opt);

        };

        this.getNetworkType = function (opt) {
            if (!me.initionaled)
                return;
            wx.getNetworkType(opt);

        };


        this.openLocation = function (opt) {
            if (!me.initionaled)
                return;
            wx.openLocation(opt);

        };

        this.getLocation = function (opt) {
            if (!me.initionaled)
                return;
            wx.getLocation(opt);

        };

        this.hideOptionMenu = function () {
            if (!me.initionaled)
                return;
            wx.hideOptionMenu();

        };

        this.showOptionMenu = function () {
            if (!me.initionaled)
                return;
            wx.showOptionMenu();

        };

        this.closeWindow = function () {
            if (!me.initionaled)
                return;
            wx.closeWindow();

        };


        this.scanQRCode = function (opt) {
            if (!me.initionaled)
                return;
            wx.scanQRCode(opt);

        };

        this.onMenuShareTimeline = function (opt) {
            if (!me.initionaled)
                return;
            wx.onMenuShareTimeline(opt);

        };


        this.onMenuShareAppMessage = function (opt) {
            if (!me.initionaled)
                return;
            wx.onMenuShareAppMessage(opt);

        };

        this.chooseWXPay = function (opt) {
            if (!me.initionaled)
                return;
            wx.chooseWXPay(opt);

        };



    }]);