angular.module('H5App.services.ShoppingCart', ['yaea.resource.shoppingcart'])
    .constant('ShoppingCartConfig', {
        localStorageKey: '74e2c5750b614692befc45be5be3647e'
    })
    .service('ShoppingCartService', ['$http', '$q', 'ShoppingCart', '$window', 'ShoppingCartConfig', 'Auth',
        function ($http, $q, ShoppingCart, $window, config, Auth) {
            var me = this;

            this.getShoppingCart = function () {
                return ShoppingCart.get().$promise;
            };

            this.saveShoppingCart = function (shoppingcart) {
                if (shoppingcart._id) {
                    return ShoppingCart.update({ id: shoppingcart._id }, shoppingcart).$promise;
                }
                else {
                    return ShoppingCart.save(shoppingcart).$promise;
                }
            };

            this.syncShoppingCartAfterLogin = function () {
                return me.getShoppingCart().then(me.mergeShoppingCart)
                    .then(me.processSaveData)
                    .then(me.saveShoppingCart)
                    .then(function (data) {
                        me.saveCartIntoLocal(data);
                    });
            };

            this.processSaveData = function (data) {
                return $q(function (resolve, reject) {
                    var cart = data.cart, newCart = [];
                    for (var index in cart) {
                        var item = cart[index];
                        item.product = item.product._id;
                        newCart.push(item);
                    }
                    data.cart = newCart;
                    resolve(data)
                });

            };

            this.mergeShoppingCart = function (data) {
                return $q(function (resolve, reject) {
                    var localCart = me.getCartFromLocal(),
                        cartArray = localCart.cart || [];
                    var mergeObj = [];
                    var cart = data.cart || [];
                    for (var index in cartArray) {
                        var item = cartArray[index];

                        var records = _.filter(cart, function (o) { return o.product._id == item.product._id; });
                        if (records.length == 0) {
                            mergeObj.push(item);
                        } else {
                            mergeObj.push(records[0]);
                        }
                    }
                    for (var index in cart) {
                        var item = cart[index];
                        var records = _.filter(cartArray, function (o) { return o.product._id == item.product._id; });
                        if (records.length == 0) {
                            mergeObj.push(item);
                        }
                    }
                    data.cart = mergeObj;
                    me.saveCartIntoLocal(data);
                    resolve(data);
                });
            }

            this.saveCartIntoLocal = function (shoppingcart) {
                var carttostring = JSON.stringify(shoppingcart);
                $window.localStorage.setItem(config.localStorageKey, carttostring);
            };

            this.getCartFromLocal = function () {
                var carttostring = $window.localStorage.getItem(config.localStorageKey);
                return JSON.parse(carttostring) || {};
            };

            this.addCart = function (item) {
                var localCart = me.getCartFromLocal(),
                    cartArray = localCart.cart || [],
                    needAdd = true;
                for (var index in cartArray) {
                    var record = cartArray[index];
                    if (record.product._id == item.product._id) {
                        record.count += item.count;
                        needAdd = false;
                        break;
                    }
                }
                if (needAdd) {
                    cartArray.push(item);
                }
                localCart.cart = cartArray;
                me.saveCartIntoLocal(localCart);
                Auth.isLoggedIn(angular.noop).then(function (is) {
                    if (is) {
                        me.processSaveData(localCart).then(me.saveShoppingCart).then(function (data) { 
                            me.saveCartIntoLocal(data);
                        });
                    }
                })
            };


            this.removeCart = function (productid) {
                var localCart = me.getCartFromLocal(),
                    cartArray = localCart.cart || [];
                _.remove(cartArray, function (n) {
                    return n.product._id == productid;
                });
                localCart.cart = cartArray;
                me.saveCartIntoLocal(localCart);
                Auth.isLoggedIn(angular.noop).then(function (is) {
                    if (is) {
                        me.processSaveData(localCart).then(me.saveShoppingCart).then(function (data) { 
                            me.saveCartIntoLocal(data);
                        });
                    }
                })
            };

            this.savelocalCarttoSever = function () {

                Auth.isLoggedIn(angular.noop).then(function (is) {
                    if (is) {
                        var localCart = me.getCartFromLocal();
                        me.processSaveData(localCart).then(me.saveShoppingCart).then(function (data) { 
                            me.saveCartIntoLocal(data);
                        });
                    }
                })
            }


        }]);