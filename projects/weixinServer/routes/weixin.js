'use strict';

var express = require('express');
var controller = require('../common/api/weixin');

var router = express.Router();

router.get('/jssdk', controller.jssdk);
router.get('/token', controller.token);



module.exports = router;
