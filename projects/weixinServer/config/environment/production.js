'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip:     process.env.IP ||
          undefined,

  // Server port
  port:   process.env.PORT ||
          8083,

  // MongoDB connection options
  mongo: {
    uri:  process.env.MONGOLAB_URI ||
          process.env.MONGOHQ_URL ||
          'mongodb://localhost/yaea'
  }
};
