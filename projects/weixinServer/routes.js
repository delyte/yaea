/**
 * Main application routes
 */

'use strict';

var errors = require('./common/common/error');
var path = require('path');

module.exports = function (app) {
    // Insert routes below
    app.use('/api/weixin', require('./routes/weixin'));
}
