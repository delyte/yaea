module.exports = {
    appNanme: 'ManagerApp',
    dest: 'www',
    cordova: false,
    less: {
        src: [
            './src/less/app.less', './src/less/responsive.less'
        ],
        paths: [
            './src/less', './bower_components'
        ]
    },
    vendor: {
        js: [
            './bower_components/angular/angular.js',
            './bower_components/angular-cookies/angular-cookies.js',
            './bower_components/angular-resource/angular-resource.js',
            './bower_components/angular-messages/angular-messages.js',
            './bower_components/ng-file-upload/ng-file-upload.js',
            './bower_components/angular-animate/angular-animate.js',
            './bower_components/angular-touch/angular-touch.js',
            './bower_components/angular-route/angular-route.js',
            './bower_components/mobile-angular-ui/dist/js/mobile-angular-ui.js',
            './bower_components/ui-bootstrap/ui-bootstrap-custom-tpls-1.2.4.js',
            './bower_components/ng-tags-input/ng-tags-input.js',
            './bower_components/lodash/lodash.js'

        ],

        css: {
            prepend: [],
            append: [
                './bower_components/bootstrap/dist/css/bootstrap.min.css'
            ],
        },

        fonts: [
            './bower_components/font-awesome/fonts/fontawesome-webfont.*'
        ]
    },

    server: {
        host: '0.0.0.0',
        port: '8000'
    },

    weinre: {
        httpPort: 8001,
        boundHost: 'localhost',
        verbose: false,
        debug: false,
        readTimeout: 5,
        deathTimeout: 15
    }
};