angular.module('ManagerApp', [
    'ngRoute',
    'mobile-angular-ui',
    'ui.bootstrap',
    'yaea.auth',
    'ngMessages',
    'ManagerApp.controllers.Main',
    'ManagerApp.controllers.Home',
    'ManagerApp.controllers.ListManage',
    'ManagerApp.controllers.NewList',
    'ManagerApp.controllers.ListDetail',
    'ManagerApp.controllers.NewProduct',
    'ManagerApp.controllers.ProductDetail',
    'ManagerApp.controllers.ProductManage',
    'ManagerApp.controllers.OrderManage',
    'ManagerApp.controllers.OrderDetail',
    'ManagerApp.controllers.SupplierManage',
    'ManagerApp.controllers.SupplierDetail',
    'ManagerApp.controllers.NewSupplier',
    'ManagerApp.directives.MainMenu',
    'ManagerApp.directives.DropDown',
    'ManagerApp.services.Product',
    'ManagerApp.services.List',
    'ManagerApp.controllers.Login',
    'ManagerApp.controllers.Signup',
    'ManagerApp.controllers.SystemDeploy'
])

    .config(
    function ($routeProvider, $locationProvider) {
        $routeProvider.when('/', { authenticate: true, controller: 'HomeController', templateUrl: 'home.html', reloadOnSearch: false })
            .when('/listmanage', { authenticate: true, controller: 'ListManageController', templateUrl: 'listmanage.html', reloadOnSearch: false })
            .when('/newlist',
            {
                authenticate: true,
                controller: 'NewListController',
                templateUrl: 'newlist.html',
                resolve: {
                    initData: function () {
                        return '';
                    },
                    isDialog: function () {
                        return false;
                    }
                },
                reloadOnSearch: false
            })
            .when('/listdetail/:id',
            {
                authenticate: true,
                controller: 'ListDetailController',
                templateUrl: 'listdetail.html',
                controllerAs: 'ListDetailCtl',
                resolve: {
                    initData: function ($route, ListService) {
                        return ListService.getListInfo($route.current.params.id);
                    }
                },
                reloadOnSearch: false
            })
            .when('/newproduct', { authenticate: true, controller: 'NewProductController', templateUrl: 'newproduct.html', reloadOnSearch: false })
            .when('/productdetail/:id',
            {
                authenticate: true,
                controller: 'ProductDetailController',
                templateUrl: 'productdetail.html',
                controllerAs: 'DetailCtl',
                resolve: {
                    initData: function ($route, ProductService) {
                        return ProductService.getProductInfo($route.current.params.id);
                    }
                }
            })
            .when('/productmanage', { authenticate: 'Admin', controller: 'ProductManageController', templateUrl: 'productmanage.html', reloadOnSearch: false })
            .when('/ordermanage', { authenticate: true, controller: 'OrderManageController', templateUrl: 'ordermanage.html', reloadOnSearch: false })
            .when('/orderdetail/:id', 
            { 
                authenticate: true, 
                controller: 'OrderDetailController', 
                templateUrl: 'orderdetail.html', 
                controllerAs: 'OrderDetailCtl',
                resolve: {
                    initData: function ($route, OrderService) {
                        return OrderService.getOrderInfo($route.current.params.id);
                    }
                },
                reloadOnSearch: false 
            })
            .when('/suppliermanage', { authenticate: true, controller: 'SupplierManageController', templateUrl: 'suppliermanage.html', reloadOnSearch: false })
            .when('/supplierdetail/:id/',
            {
                controller: 'SupplierDetailController',
                templateUrl: 'supplierdetail.html',
                reloadOnSearch: false,
                resolve: {
                    initData: function ($route, SupplierService) {
                        return SupplierService.showSupplier($route.current.params.id);
                    }
                }
            })
            .when('/newsupplier', { authenticate: true, controller: 'NewSupplierController', templateUrl: 'newsupplier.html', reloadOnSearch: false })
            .when('/login', { controller: 'LoginController', templateUrl: 'login.html', reloadOnSearch: false })
            .when('/signup', { controller: 'SignupController', templateUrl: 'signup.html', reloadOnSearch: false })
            .when('/systemdeploy', { authenticate: true, controller: 'SystemDeployController', templateUrl: 'systemdeploy.html', reloadOnSearch: false });
        $locationProvider.html5Mode(true);
    });