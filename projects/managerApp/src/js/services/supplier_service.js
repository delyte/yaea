angular.module('ManagerApp.services.Supplier', ['yaea.resource.supplier'])
    .service('SupplierService', ['$http', '$q', 'Supplier', function ($http, $q, Supplier) {
        this.getSuppliers = function (query, pageindex) {
            return Supplier.query(
                {
                    'pageindex': pageindex || 1,
                    'query': query
                }).$promise;
        };

        this.showSupplier = function (id) {
            return Supplier.get({ id: id }).$promise;
        };

        this.createSupplier = function (supplier) {
            return Supplier.save(supplier).$promise;
        };

        this.deleteSupplier = function (id) {
            return Supplier.delete({ id: id }).$promise;
        };

        this.supplier = function () {
            return Supplier.supplier().$promise;
        }

    }]);
