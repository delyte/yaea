angular.module('ManagerApp.services.Page', ['yaea.resource.page'])
    .service('PageService', ['$http', '$q', 'Page', function ($http, $q, Page) {
        this.savePage = function (page) {
            if (page.id) {
                return Page.update({ id: page.id }, page).$promise;
            }
            else {
                return Page.save(page).$promise;
            }

        };


        this.getPage = function (type) {
            return Page.showPage({ type: type }).$promise;
        };




    }]);