angular.module('ManagerApp.services.List', ['yaea.resource.list'])
    .service('ListService', ['$http', '$q', 'List', function($http, $q, List) {
        this.saveList = function(list) {
            return List.save(list).$promise;
        };

        this.showList = function(query, pageindex) {
            return List.query(
                {
                    'pageindex': pageindex || 1,
                    'query': query

                }).$promise;
        };

        this.getListInfo = function(id) {
            return List.get({ id: id }).$promise;
        };

        this.list = function() {
            return List.list().$promise;
        };

        this.update = function(id, list) {
            return List.update({ id: id }, list).$promise;
        };
        
        this.labels = function(){
            return List.label().$promise;
        };
        
        this.deleteList = function (id) {
            return List.delete({ id: id }).$promise;
        };


    }]);