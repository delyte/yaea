angular.module('ManagerApp.services.Utility', [])
    .constant('UtilityServiceConfig', {
        photoGalleryNamesUrl: 'api/utility/photoGalleryNames',
    })
    .service('UtilityService', [
        '$http',
        '$q',
        'UtilityServiceConfig',
        'appConfig',
        function ($http, $q, UtilityServiceConfig,appConfig) {
            this.photoGalleryNames = function () {
                var req = {
                    method: 'GET',
                    url: UtilityServiceConfig.photoGalleryNamesUrl
                };

                return $http(req)
            };

            this.codetable = function () {
                var defer = $q.defer();
                defer.resolve(appConfig.codeTable);
                return defer.promise;
            };
        }]);
