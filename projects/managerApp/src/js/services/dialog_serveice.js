'use strict';
angular.module('ManagerApp.services.Dialog', ['ManagerApp.controllers.AttachmentUploadTemplate',
    'ManagerApp.controllers.ConfirmDeleteTemplate', 'ManagerApp.controllers.SaveAsListTemplate',
    'ManagerApp.controllers.NewList','ManagerApp.controllers.WarningDialog'])
    .constant('DialogServiceConfig', {
        UploadingDialog: {
            templateUrl: 'attachmentuploadtemplate.html',
            controller: 'AttachmentUploadTemplateController',
            controllerAs: 'dialogCtrl'

        },
        ConfirmDeleteDialog: {
            templateUrl: 'confirmdeletetemplate.html',
            controller: 'ConfirmDeleteTemplateController',
            controllerAs: 'dialogCtrl'
        },
        SaveAsListDialog: {
            templateUrl: 'saveaslist.html',
            controller: 'SaveAsListTemplateController',
            controllerAs: 'dialogCtrl'
        },
        NewListDialog: {
            templateUrl: 'newlistdialogtemplate.html',
            controller: 'NewListController',
            controllerAs: 'dialogCtrl'
        },
        WarningDialog: {
            templateUrl: 'warningdialog.html',
            controller: 'WarningDialogController',
            controllerAs: 'dialogCtrl'
        }
    })
    .service('DialogService', ['$uibModal', 'DialogServiceConfig', function ($modal, config) {
        this.showUploadingDialog = function (size) {
            var modalInstance = $modal.open({
                templateUrl: config.UploadingDialog.templateUrl,
                controller: config.UploadingDialog.controller,
                size: size,
                controllerAs: config.UploadingDialog.controllerAs
            });

            return modalInstance.result;
        };

        this.showConfirmDeleteDialog = function () {
            var modalInstance = $modal.open({
                templateUrl: config.ConfirmDeleteDialog.templateUrl,
                controller: config.ConfirmDeleteDialog.controller,
                controllerAs: config.ConfirmDeleteDialog.controllerAs,
                size: 'sm'
            });
            return modalInstance.result;
        };

        this.showWarningDialog = function (title, content) {
            var modalInstance = $modal.open({
                templateUrl: config.WarningDialog.templateUrl,
                controller: config.WarningDialog.controller,
                controllerAs: config.WarningDialog.controllerAs,
                size: 'sm',
                resolve: {
                    dataInstance: function () {
                        return {
                            title: title,
                            content: content
                        }
                    }
                },
            });
            return modalInstance.result;
        };

        this.showSaveAsListDialog = function (query) {
            var modalInstance = $modal.open({
                templateUrl: config.SaveAsListDialog.templateUrl,
                controller: config.SaveAsListDialog.controller,
                controllerAs: config.SaveAsListDialog.controllerAs,
                resolve: {
                    query: function () {
                        return query;
                    }
                }
            });
            return modalInstance.result;

        };

        this.showNewListDialog = function (newListinfo) {
            var modalInstance = $modal.open({
                templateUrl: config.NewListDialog.templateUrl,
                controller: config.NewListDialog.controller,
                controllerAs: config.NewListDialog.controllerAs,
                size: 'lg',
                resolve: {
                    initData: function () {
                        return newListinfo;
                    },
                    isDialog: function () {
                        return true;
                    }
                }
            });
            return modalInstance.result;

        };

    }]);