angular.module('ManagerApp.services.Product', ['yaea.resource.product'])

    .service('ProductService', ['$http', '$q', 'Product', function($http, $q, Product) {
        this.saveProduct = function(product) {
            return Product.save(product).$promise;
        };


        this.showProduct = function(query, pageindex) {
            return Product.query(
                {
                    'pageindex': pageindex || 1,
                    'query': query

                }).$promise;

        };

        this.getProductInfo = function(id) {
            return Product.get({ id: id }).$promise;

        };

        this.convertTagObject = function(product, tagArray) {
            var defer = $q.defer();

            for (key in tagArray) {
                var item = tagArray[key];
                for (var i = 0; i < item.length; i++) {
                    product[key].push(item[i].text)
                }
            }
            defer.resolve(product);
            return defer.promise;
        };
        
        this.update = function(id, product) {
            return Product.update({ id: id }, product).$promise;
        };
        
        this.labels = function(){
            return Product.labels().$promise;
        };
        
        this.getSearchSetup=function() {
             return Product.getSearchSetup().$promise;
        };
        
        this.deleteProduct = function (id) {
            return Product.delete({ id: id }).$promise;
        };
        
        
    }]);
