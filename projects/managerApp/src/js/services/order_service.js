angular.module('ManagerApp.services.Order', ['yaea.resource.order'])
    .service('OrderService', ['$http', '$q', 'Order', function ($http, $q, Order) {
        this.showOrder = function(query, pageindex) {
            return Order.query(
                {
                    'pageindex': pageindex || 1,
                    'query': query

                }).$promise;
        };
        
        this.getOrderInfo = function(id) {
            return Order.get({ id: id }).$promise;

        };




    }]);