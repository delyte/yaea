

angular
    .module('ManagerApp.directives.DropDown', [])
    .controller('YaeaDropdownController', ['$scope', function ($scope) {
        var ngModelCtrl = { $setViewValue: angular.noop },
            self = this;
        $scope.selectedItems = [];
        self.title = '';
        self.init = function (ngModelCtrl_) {
            self.title = $scope.title;
            ngModelCtrl = ngModelCtrl_;
            ngModelCtrl.$setViewValue($scope.selectedItems);
            ngModelCtrl.$render = self.render;
        };

        self.checkItem = function (value, item) {
            if (value) {
                $scope.selectedItems.push(item);
            }
            else {
                for (var index in $scope.selectedItems) {
                    var deleteItem = $scope.selectedItems[index];
                    if (deleteItem == item) {
                        $scope.selectedItems.splice(index, 1);
                        break;
                    }
                }
            }
            ngModelCtrl.$setViewValue($scope.selectedItems);
        };

        self.render = function () {
            $scope.selectedItems = ngModelCtrl.$viewValue;
        };

        self.onclick = function () {
            if ($scope.selectedItems.length > 0) {
                self.title = $scope.selectedItems.join(',');
            }
            else {
                self.title = $scope.title;
            }
            $scope.yaeaClick();
        }

    }])
    .directive('yaeaDropdown', function () {
        var directive = {
            controller: 'YaeaDropdownController',
            controllerAs: 'vm',
            link: link,
            replace: true,
            templateUrl: 'dropdownTemplate_directive.html',
            restrict: 'AE',
            require: ['yaeaDropdown', 'ngModel'],
            scope: {
                title: '@', yaeaState: '@', dateModel: '=', yaeaClick: "&"
            }
        };
        return directive;

        function link(scope, element, attrs, ctrls) {
            var YaeaDropDownCtrl = ctrls[0], ngModelCtrl = ctrls[1];
            YaeaDropDownCtrl.init(ngModelCtrl);
        }
    });
