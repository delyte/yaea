angular.module('ManagerApp.directives.DatePicker', [])
    .controller('YaeaDatePickerController', ['$scope', '$attrs', '$location', function($scope, $attrs, $location) {
        var ngModelCtrl = { $setViewValue: angular.noop },
            self = this;



        $scope.altInputFormats = ['M!/d!/yyyy'];
        $scope.dateOptions = {
            // dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        self.open = function() {
            $scope.opened = true;
        };


    }])
    .directive('yaeaDatepicker', function() {
        return {
            scope: { dateFormat: '@', dateModel: '=' },
            require: ['yaeaDatepicker'],
            restrict: 'AE',
            replace: true,
            controller: 'YaeaDatePickerController',
            templateUrl: 'datepickerTemplate_directive.html',
            controllerAs: 'datepickerCtrl',
            link: function(scope, element, attrs, ctrls) {
                if (attrs.style) {
                    element.attr.apply(attrs.style);
                }
                if (attrs.class) {
                    element.attr.apply(attrs.class);
                }
            }
        }
    });