angular.module('ManagerApp.controllers.Main', ["ManagerApp.services.Utility"])

    .controller('MainController', ['$scope', 'UtilityService', 'Auth', '$location', function($scope, UtilityService, Auth, $location) {
        UtilityService.codetable().then(function(data) {
            $scope.codetable = data;
        });

        $scope.$on("$routeChangeStart", function(event, next, current) {
            var nextRout = next.$$route;
            if (!nextRout.authenticate) {
                return;
            }

            if (typeof nextRout.authenticate === 'string') {
                Auth.hasRole(nextRout.authenticate, angular.noop).then(function(has) {
                    if (has) {
                        return;
                    }

                    event.preventDefault();
                    return Auth.isLoggedIn(angular.noop).then(function(is) {
                        $location.path(is ? '/' : '/login');
                    });
                });
            } else {
                Auth.isLoggedIn(angular.noop).then(function(is) {
                    if (is) {
                        return;
                    }

                    event.preventDefault();
                    $location.path('/login');
                });
            }



        });

    }]);