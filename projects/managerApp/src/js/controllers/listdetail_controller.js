angular.module('ManagerApp.controllers.ListDetail', [
    'ManagerApp.services.Dialog',
    'ManagerApp.directives.DatePicker',
    'ngTagsInput',
    'ManagerApp.services.List'
])

    .controller('ListDetailController', ['$scope', 'DialogService', 'ListService', '$location', 'initData', function ($scope, DialogService, ListService, $location, initData) {
        $scope.isEdit = false;
        $scope.listinfo = initData || {
            name: '',
            sn: '',
            type: '',
            status: '',
            startTime: '',
            endTime: '',
            description: '',
            cover: [],
            attachments: [],
            labels: []
        };

        this.toggleOperate = function (params) {
            $scope.isEdit = !$scope.isEdit
        }

        $scope.uploadAttachments = function () {
            DialogService.showUploadingDialog().then(function (data) {
                $scope.listinfo.attachments = $scope.listinfo.attachments.concat(data);
            })
        };

        $scope.removeAttachment = function (index) {
            $scope.listinfo.attachments.splice(index, 1);
        };

        $scope.uploadCovers = function () {
            DialogService.showUploadingDialog().then(function (data) {
                $scope.listinfo.cover = $scope.listinfo.cover.concat(data);
            })
        };

        $scope.removeCover = function (index) {
            $scope.listinfo.cover.splice(index, 1);
        };

        $scope.update = function () {
            if ($scope.listinfo.cover.length > 0) {
                ListService.update($scope.listinfo.id, $scope.listinfo)
                    .then(function (data) {
                        $location.path('/listmanage')
                    });
            } else {
                var title = '警告';
                var content = '列表封面不能为空！';
                DialogService.showWarningDialog(title, content).then(function (data) {

                });
            }

        };


    }]);