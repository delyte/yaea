
angular.module('ManagerApp.controllers.NewProduct',
    [
        'ManagerApp.services.Dialog',
        'ManagerApp.directives.DatePicker',
        'ngTagsInput',
        'ManagerApp.services.Product',
        'ManagerApp.services.Supplier'
    ])

    .controller('NewProductController',
    [
        '$scope',
        'DialogService',
        'ProductService',
        '$location',
        'SupplierService',
        function ($scope, DialogService, ProductService, $location, SupplierService) {

            $scope.productService = ProductService;


            SupplierService.supplier().then(function (data) {
                $scope.supplier = data;
            });

            $scope.newProduct = {
                name: '',
                price: 0,
                inStock: 0,
                sold: 0,
                origin: '',
                onlineTime: new Date(),
                supplier: '',
                labels: [],
                description: '',
                cover: [],
                images: []
            }
            $scope.productCovers = [];
            $scope.uploadCovers = function () {
                DialogService.showUploadingDialog().then(function (data) {
                    $scope.newProduct.cover = $scope.newProduct.cover.concat(data);
                })
            };

            $scope.removeCover = function (index) {
                $scope.newProduct.cover.splice(index, 1);
            };

            $scope.productImages = [];

            $scope.uploadImages = function () {
                DialogService.showUploadingDialog().then(function (data) {
                    $scope.newProduct.images = $scope.newProduct.images.concat(data);
                })
            };

            $scope.removeImage = function (index) {
                $scope.newProduct.images.splice(index, 1);
            };

            $scope.save = function () {
                if ($scope.newProduct.cover.length > 0) {
                    ProductService.saveProduct($scope.newProduct)
                        .then(function (data) {
                            $location.path('/productmanage')
                        });
                } else {
                    var title = '警告';
                    var content = '商品封面不能为空！';
                    DialogService.showWarningDialog(title, content).then(function (data) {

                    });
                }
            };
        }]);
