angular.module('ManagerApp.controllers.SupplierDetail', [])

    .controller('SupplierDetailController', function($scope, initData) {

        $scope.isEdit = false;
        
        $scope.toggleOperate = function(params) {
            $scope.isEdit = !$scope.isEdit
        }

        $scope.supplierinfo = initData;

     
    });