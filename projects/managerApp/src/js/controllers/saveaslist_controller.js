angular.module('ManagerApp.controllers.SaveAsListTemplate', [])
    .controller('SaveAsListTemplateController', [
        '$scope', '$uibModalInstance', 'query',
        function ($scope, $modalInstance, query) {
            var self = this;

            self.ok = function () {
                //precheck whether list exists
                $modalInstance.close({ name: $scope.newListName, query: query });
            };

            self.cancel = function () {
                $modalInstance.dismiss(false);
            };


        }]);