angular.module('ManagerApp.controllers.OrderManage', [
    'ManagerApp.services.Order'
])

    .controller('OrderManageController', function ($scope,OrderService,$location) {
        $scope.query = {};
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.orderdatas = [];
        
        OrderService.showOrder($scope.query).then(function (data) {
            $scope.orderdatas = _.toArray(data[0]);
            $scope.totalItems = data[1]
        });
        
        $scope.pageChange = function () {
            OrderService.showOrder($scope.query, $scope.currentPage).then(function (data) {
                $scope.orderdatas = _.toArray(data[0]);
                $scope.totalItems = data[1]
            });
        };
        
        $scope.dropdowntitle = '订单状态';
        
        $scope.statuslist = [{
            id:999,
            text:'全部'
        },{
            id:'0',
            text:'待付款'
        },{
            id:'1',
            text:'待收货'
        },{
            id:2,
            text:'待评价'
        },{
            id:3,
            text:'已完成'
        }];
        
        $scope.changeTitle = function(item){
            $scope.dropdowntitle = item.text;
        };
        
        $scope.selectOrder = function(id){
            $location.path('/orderdetail/' + id);
        };
        
        
    });