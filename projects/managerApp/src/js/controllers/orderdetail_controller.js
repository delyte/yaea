angular.module('ManagerApp.controllers.OrderDetail', [])

    .controller('OrderDetailController', function ($scope,initData) {
        
        $scope.isEdit = false;
        $scope.orderinfo = initData || {
            user: {
                name:''
            },
            sn: '',
            status: '',
            orderedOn:'',
            recipient: {
                name:'',
                phone:'',
                address:''
            },
            orders: [
                {
                    product:{
                        sn:'',
                        name:'',
                        origin:'',
                        onlineTime:''
                    },
                    count:0
                }
            ],
            bill: {
                productBill: 0,
                carriageBill: 0
            },
            remark: ''
        };

        this.toggleOperate = function (params) {
            $scope.isEdit = !$scope.isEdit
        };
        
        // $scope.orderinfo = {
        //     ordernum: 'YaeaOrd-17',
        //     ordername: '三八女人节五折优惠车厘子',
        //     orderdetail: {
        //         status: '完成',
        //         date: '2016-02-19'
        //     },
        //     recipientinfo: {
        //         name: '&218952#',
        //         phonenum: '13786542345',
        //         address: '当初觉得你决定多层次'
        //     },
        //     childorder: [],
        //     productdetails: [
        //         {
        //             productnum: 'yaea-17',
        //             productname: '三八节活动热销产品',
        //             operatetype: '上架',
        //             operatetime: '2016-03-30'
        //         }
        //     ],
        //     ordersettlement: {
        //         productsum: 69.00,
        //         freight: 6.00,
        //         totalsum:75.00
        //     }
        //     ,
        //     operatehistorys: [
        //         {
        //             ordernum: 'YaeaOrder-17',
        //             ordertime: '2016-03-12 13:10:22',
        //             subordinatetolist: '水果',
        //             type: '活动',
        //             status: '可用'

        //         }
        //     ]
        // };
    });