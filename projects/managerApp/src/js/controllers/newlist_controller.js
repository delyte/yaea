angular.module('ManagerApp.controllers.NewList', [
    'ManagerApp.services.Dialog',
    'ManagerApp.directives.DatePicker',
    'ngTagsInput',
    'ManagerApp.services.List'])

    .controller('NewListController',
    ['$scope', 'DialogService', 'ListService', '$location', 'initData', 'isDialog', 'UtilityService',
        function ($scope, DialogService, ListService, $location, initData, isDialog, UtilityService) {
            var self = this;
            $scope.listService = ListService;
            $scope.newList = {
                name: initData && initData.name || '',
                type: '',
                status: '',
                startTime: new Date(),
                endTime: new Date(),
                description: '',
                attachments: [],
                productRelation: initData && initData.query && JSON.stringify(initData.query) || '',
                cover: [],
                labels: []
            };
            if (isDialog) {
                UtilityService.codetable().then(function (data) {
                    $scope.codetable = data;

                });
            }
            $scope.isDialog = isDialog;


            $scope.uploadAttachments = function () {
                DialogService.showUploadingDialog('lg').then(function (data) {
                    $scope.newList.attachments = $scope.newList.attachments.concat(data);
                })
            };

            $scope.removeAttachment = function (index) {
                $scope.newList.attachments.splice(index, 1)
            };

            $scope.uploadCovers = function () {
                DialogService.showUploadingDialog('lg').then(function (data) {
                    $scope.newList.cover = $scope.newList.cover.concat(data);
                })
            };

            $scope.removeCover = function (index) {
                $scope.newList.cover.splice(index, 1);
            };

            $scope.save = function () {
                if ($scope.newList.startTime > $scope.newList.endTime) {
                    var title = '警告';
                    var content = '结束时间不能早于开始时间！';
                    DialogService.showWarningDialog(title, content);
                    return;
                }
                if ($scope.newList.cover.length > 0) {
                    ListService.saveList($scope.newList)
                        .then(function (data) {
                            $location.path('/listmanage')
                        });
                } else {
                    var title = '警告';
                    var content = '列表封面不能为空！';
                    DialogService.showWarningDialog(title, content);
                }

            };

            self.ok = function () {
                ListService.saveList($scope.newList)
                    .then(function (data) {
                        $scope.$close(data);
                    });

            };

            self.cancel = function () {
                $scope.$dismiss();
            };

        }]);