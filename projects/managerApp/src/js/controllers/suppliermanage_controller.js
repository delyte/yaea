angular.module('ManagerApp.controllers.SupplierManage', ['ManagerApp.services.Supplier', 'ManagerApp.services.Dialog'])

    .controller('SupplierManageController', function($scope, $location, SupplierService, DialogService) {
        $scope.selectSupplier = function(id) {
            $location.path('/supplierdetail/' + id);
        };

        $scope.searchSupplier = function() {
            var keyword = $scope.keyword, query = '';
            if (keyword) {
                query = { $text: { $search: keyword } };
            }
            SupplierService.getSuppliers(query).then(function(data) {
                $scope.suppliers = _.toArray(data[0]);
                $scope.totalItems = data[1];
            });
        };

        $scope.deleteSupplier = function(id) {
            DialogService.showConfirmDeleteDialog().then(function(data) {
                if (data) {
                    SupplierService.deleteSupplier(id).then(function() {
                        _.remove($scope.suppliers, function(item) {
                            return item.id == id;
                        });
                    });
                }
            });

        }

    });
