angular.module('ManagerApp.controllers.AttachmentUploadTemplate', ['ngFileUpload', 'ManagerApp.services.Utility'])

    .controller('AttachmentUploadTemplateController', [
        '$scope', '$uibModalInstance', 'Upload', '$timeout', 'UtilityService',
        function($scope, $modalInstance, Upload, $timeout, UtilityService) {
            var self = this;
            $scope.allattachments = [];
            UtilityService.photoGalleryNames().then(function(response) {
                $scope.allattachments = response.data;
            })
            $scope.listattachments = [];

            self.attachmentNames = [];
            self.uploadFiles = function(listattachments, errFiles) {
                $scope.listattachments = $scope.listattachments.concat(listattachments);
                $scope.errFiles = errFiles;
                angular.forEach(listattachments, function(file) {
                    file.fieldname = file.fieldname + '.jpeg'
                    file.upload = Upload.upload({
                        url: '/api/utility/upload',
                        data: { file: file }
                    });
                    file.show = true;
                    file.upload.then(function(response) {
                        $timeout(function() {
                            file.show = false;
                            $scope.allattachments = $scope.allattachments.concat(response.data.fileName);
                        });
                    }, function(response) {
                        if (response.status > 0) {
                            $scope.errorMsg = response.status + ':' + response.data;
                        }
                    }, function(evt) {
                        file.progress = Math.min(100, parseInt(100 * evt.loaded / evt.total));
                    });

                });
            };

            self.ok = function() {
                $modalInstance.close(self.attachmentNames);
            };

            self.cancel = function() {
                $modalInstance.dismiss();
            };

            self.checkImage = function(value, imageName) {
                if (value) {
                    self.attachmentNames.push(imageName);
                }
                else {
                    for (var index in self.attachmentNames) {
                        var deleteItem = self.attachmentNames[index];
                        if (deleteItem == imageName) {
                            self.attachmentNames.splice(index, 1);
                            break;
                        }
                    }
                }
            }
        }]);