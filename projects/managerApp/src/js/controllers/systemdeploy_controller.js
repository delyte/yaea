angular.module('ManagerApp.controllers.SystemDeploy', [
    'ManagerApp.services.List', 
    'ManagerApp.services.Page',
    'ManagerApp.services.Dialog'
    ])
    .controller('SystemDeployController',
    [
        '$scope',
        'Auth',
        '$location',
        'ListService',
        'PageService',
        'DialogService',
        function ($scope, Auth, $location, ListService, PageService,DialogService) {
            var self = this;
            $scope.lists = [];
            ListService.list().then(function (data) {
                $scope.lists = data;
            });
            self.pageInfo = {
                type: 'Main',
                layout: [
                ]
            };

            self.layoutInfo = {
                Top: [],
                Tag: [],
                Body: []
            };


            $scope.mappingLists = null;

            PageService.getPage('Main').then(function (data) {
                self.pageInfo = data;
                for (var key in self.layoutInfo) {
                    var layout = _.find(data.layout, function (item) {
                        return item.type == key;
                    });
                    self.layoutInfo[key] = layout && layout.lists || [];
                }
            });

            $scope.change = function (code) {

                $scope.mappingLists = self.layoutInfo[code]

            }

            $scope.changeItem = function (list) {
                $scope.mappingItem = list;
            };

            $scope.mappingTo = function () {

                if ($scope.mappingLists && $scope.mappingItem) {
                    if (_.filter($scope.mappingLists, function (o) { return o.id == $scope.mappingItem.id; }).length == 0) {
                        $scope.mappingLists.push($scope.mappingItem);
                    }

                }
            };


            $scope.remove = function (index) {
                $scope.mappingLists.splice(index, 1);
            }
            $scope.save = function () {
                var layout = [];
                for (var key in self.layoutInfo) {
                    layout.push({
                        type: key, lists: self.layoutInfo[key]
                    })
                }
                self.pageInfo.layout = layout;
                PageService.savePage(self.pageInfo).then(
                    function (data) {
                        var title = '提示';
                        var content = '保存成功！';
                        DialogService.showWarningDialog(title, content).then(function (data) {

                        });
                    }
                );
            };
        }
    ]);