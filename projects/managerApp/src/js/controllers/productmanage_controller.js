angular.module('ManagerApp.controllers.ProductManage', [
    'ManagerApp.services.Dialog',
    'ManagerApp.services.Product',
    'ManagerApp.services.List'])

    .controller('ProductManageController', function ($scope, ProductService, ListService, DialogService, $location) {
        $scope.searchText = '';
        $scope.listService = ListService;
        $scope.productdatas = [];
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.searchsetup = [];
        $scope.labelsearch = [];
        $scope.originsearch = [];

        ProductService.showProduct($scope.query).then(function (data) {
            $scope.productdatas = _.toArray(data[0]);
            $scope.totalItems = data[1]
        });
        ProductService.getSearchSetup().then(function (data) {
            $scope.searchsetup = data;
        });
        $scope.pageChange = function () {
            ProductService.showProduct($scope.query, $scope.currentPage).then(function (data) {
                $scope.productdatas = _.toArray(data[0]);
                $scope.totalItems = data[1]
            });
        };

        $scope.selectProduct = function (id) {
            $location.path('/productdetail/' + id);
        };

        $scope.saveAsList = function () {
            var query = {};
            if ($scope.labelsearch.length > 0) {
                _.merge(query, { 'labels.text': { '$in': $scope.labelsearch } });
            }
            if ($scope.originsearch.length > 0) {
                _.merge(query, { 'origin': { '$in': $scope.originsearch } });
            }
            DialogService.showSaveAsListDialog(query).then(function (data) {
                var newListinfo = data;
                if (newListinfo) {
                    DialogService.showNewListDialog(newListinfo);
                }
            });
        }

        $scope.search = function () {

            var query = {}, searchText = '';
            if ($scope.labelsearch.length > 0) {
                _.merge(query, { 'labels.text': { '$in': $scope.labelsearch } });
                //{ 'labels.text':{ '$in':$scope.labelsearch } }
            }
            if ($scope.originsearch.length > 0) {
                _.merge(query, { 'origin': { '$in': $scope.originsearch } });
            }
            if ($scope.searchText) {
                _.merge(query, { $text: { $search: $scope.searchText } });
            }

            ProductService.showProduct(query, $scope.currentPage).then(function (data) {
                $scope.productdatas = _.toArray(data[0]);
                $scope.totalItems = data[1]
            });


        };
        
        $scope.deleteProduct = function(id) {
            DialogService.showConfirmDeleteDialog().then(function(data) {
                if (data) {
                    ProductService.deleteProduct(id).then(function() {
                        _.remove($scope.productdatas, function(item) {
                            return item.id == id;
                        });
                    });
                }
            });

        }


    });