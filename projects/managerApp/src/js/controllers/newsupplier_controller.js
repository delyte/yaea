angular.module('ManagerApp.controllers.NewSupplier', [
    'ManagerApp.services.Dialog',
    'ngTagsInput',
    'ManagerApp.services.Supplier',
    'ManagerApp.services.Utility',
])
    .controller('NewSupplierController', function ($scope, $location, SupplierService, UtilityService, DialogService) {


        $scope.supplierCovers = [];
        $scope.newSupplier = {
            cover: []
        }

        $scope.uploadCovers = function () {
            DialogService.showUploadingDialog().then(function (data) {
                $scope.newSupplier.cover = $scope.newSupplier.cover.concat(data);
            })
        };

        $scope.removeCover = function (index) {
            $scope.newSupplier.cover.splice(index, 1);
        };


        $scope.save = function () {
            if ($scope.newSupplier.cover.length > 0) {
                SupplierService.createSupplier($scope.newSupplier)
                    .then(function (data) {
                        $location.path('/suppliermanage')
                    });
            } else {
                var title = '警告';
                var content = '供应商封面不能为空！';
                DialogService.showWarningDialog(title, content).then(function (data) {

                });
            }
        }


    });