angular.module('ManagerApp.controllers.Login', [])

    .controller('LoginController', ['$scope', 'Auth', '$location', function($scope, Auth, $location) {
        $scope.user = {
            phonenumber: '',
            password: ''
        };

        $scope.login = function() {
            Auth.login($scope.user).then(function(data) {
                $location.path('/')
            });
        }

    }]);