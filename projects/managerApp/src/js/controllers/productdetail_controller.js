angular.module('ManagerApp.controllers.ProductDetail', [
    'ngTagsInput',
    'ManagerApp.services.Supplier',
    'ManagerApp.services.Product',
    'ManagerApp.services.Dialog'
])

    .controller('ProductDetailController', function ($scope, DialogService, ProductService, SupplierService, initData, $location) {
        $scope.isEdit = false;

        SupplierService.supplier().then(function (data) {
            $scope.supplier = data;
        });
        $scope.productinfo = initData || {
            sn: '',
            name: '',
            price: 0,
            inStock: 0,
            sold: 0,
            origin: '',
            onlineTime: new Date(),
            lists: [],
            supplier: '',
            labels: [],
            description: '',
            cover: [],
            images: []
        };

        this.toggleOperate = function (params) {
            $scope.isEdit = !$scope.isEdit
        };

        $scope.uploadCovers = function () {
            DialogService.showUploadingDialog().then(function (data) {
                $scope.productinfo.cover = $scope.productinfo.cover.concat(data);
            })
        };

        $scope.removeCover = function (index) {
            $scope.productinfo.cover.splice(index, 1);
        };

        $scope.uploadImages = function () {
            DialogService.showUploadingDialog('sm').then(function (data) {
                $scope.productinfo.images = $scope.productinfo.images.concat(data);
            })
        };

        $scope.removeImage = function (index) {
            $scope.productinfo.images.splice(index, 1);
        };

        $scope.update = function () {
            if ($scope.productinfo.cover.length > 0) {
                ProductService.update($scope.productinfo.id, $scope.productinfo)
                    .then(function (data) {
                        $location.path('/productmanage')
                    });
            } else {
                var title = '警告';
                var content = '商品封面不能为空！';
                DialogService.showWarningDialog(title, content).then(function (data) {

                });
            }

        };

    });