angular.module('ManagerApp.controllers.ListManage', [
    'ManagerApp.services.List',
    'ManagerApp.services.Dialog'
])

    .controller('ListManageController', function ($scope,DialogService, ListService, $location) {
        $scope.query = {};
        $scope.listdatas = [];
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        ListService.showList($scope.query).then(function (data) {
            $scope.listdatas = _.toArray(data[0]);
            $scope.totalItems = data[1]
        });
        $scope.pageChange = function () {
            ListService.showList($scope.query, $scope.currentPage).then(function (data) {
                $scope.listdatas = _.toArray(data[0]);
                $scope.totalItems = data[1]
            });
        };

        $scope.selectList = function (id) {
            $location.path('/listdetail/' + id);
        };

        $scope.search = function () {
            var query;
            if ($scope.searchText) {
                query = {
                    $text: { $search: $scope.searchText }
                }
            }
            else {
                query = $scope.query;
            }
            ListService.showList(query, $scope.currentPage).then(function (data) {
                $scope.listdatas = _.toArray(data[0]);
                $scope.totalItems = data[1]
            });

        };
        
        $scope.deleteList = function(id) {
            DialogService.showConfirmDeleteDialog().then(function(data) {
                if (data) {
                    ListService.deleteList(id).then(function() {
                        _.remove($scope.listdatas, function(item) {
                            return item.id == id;
                        });
                    });
                }
            });

        }


    });