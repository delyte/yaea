angular.module('ManagerApp.controllers.ConfirmDeleteTemplate', [])
    .controller('ConfirmDeleteTemplateController', [
        '$scope', '$uibModalInstance',
        function($scope, $modalInstance) {
            var self = this;

            self.ok = function() {
                $modalInstance.close(true);
            };

            self.cancel = function() {
                $modalInstance.dismiss(false);
            };


        }]);