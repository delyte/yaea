angular.module('ManagerApp.controllers.WarningDialog', [])
    .controller('WarningDialogController', [
        '$scope', '$uibModalInstance', 'dataInstance',
        function ($scope, $modalInstance, dataInstance) {
            var self = this;

            $scope.title = dataInstance.title;
            $scope.content = dataInstance.content;
            self.ok = function () {
                $modalInstance.close(true);
            };

            self.cancel = function () {
                $modalInstance.dismiss(false);
            };


        }]);