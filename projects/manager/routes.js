/**
 * Main application routes
 */

'use strict';

var errors = require('./common/common/error');
var path = require('path');

module.exports = function(app) {
    // Insert routes below
    app.use('/api/product', require('./routes/product'));
    app.use('/api/supplier', require('./routes/supplier'));
    app.use('/api/page', require('./routes/page'));

    app.use('/api/utility', require('./routes/utility'));
    app.use('/api/list', require('./routes/list'));
    app.use('/api/user', require('./routes/user'));
    app.use('/auth', require('./common/auth'));
    app.use('/api/order', require('./routes/order'));

    /* nginx will host routes
    
       // All undefined asset or api routes should return a 404
       app.route('/:url(api|auth|components|app|bower_components|assets)/*')
           .get(errors[404]);
           */

    //All other routes should redirect to the index.html
    app.route('/*')
        .get(function(req, res) {
            res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
        });



}
