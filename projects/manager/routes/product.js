'use strict';

var express = require('express');
var controller = require('../common/api/product');
var auth = require('../common/auth/service');

var router = express.Router();

router.get('/', controller.index);
router.get('/labels',controller.labels);
router.get('/searchsetup',controller.searchsetup);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
