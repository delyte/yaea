'use strict';

var express = require('express');

var controller = require('../../component/server/api/utility');

var router = express.Router();
var path = require('path');
var config = require('../config/environment');
var multer = require('multer')

var multerFolder = config.uploadFolder;
router.post('/upload', multer({ dest: multerFolder }).single('file'), controller.upload);

router.get('/photoGalleryNames', controller.photoGalleryNames(multerFolder));



module.exports = router;
