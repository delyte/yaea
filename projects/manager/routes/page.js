'use strict';

var express = require('express');
var controller = require('../common/api/page');
var auth = require('../common/auth/service');

var router = express.Router();

router.get('/type', controller.show);
router.put('/:id', controller.update);
router.post('/', controller.create);

module.exports = router;
