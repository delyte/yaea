/**
 * Main application routes
 */

'use strict';

var errors = require('../component/server/common/error');
var path = require('path');

module.exports = function (app) {
    // Insert routes below
    app.use('/api/product', require('./routes/product'));
    app.use('/api/page', require('./routes/page'));
    app.use('/api/user', require('./routes/user'));
    app.use('/api/shoppingcart', require('./routes/shoppingcart'));
    app.use('/api/recipient', require('./routes/recipient'));
    app.use('/api/order', require('./routes/order'));
    app.use('/api/weixin', require('./routes/weixin'));

    app.use('/auth', require('./common/auth'));

    /* nginx will host routes
    
       // All undefined asset or api routes should return a 404
       app.route('/:url(api|auth|components|app|bower_components|assets)/*')
           .get(errors[404]);
           */

    //All other routes should redirect to the index.html
    app.route('/*')
        .get((req, res) => {
            res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
        });



}
