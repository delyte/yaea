'use strict';

var express = require('express');
var controller = require('../common/api/shoppingcart');
var auth = require('../common/auth/service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);

module.exports = router;
