'use strict';

var express = require('express');
var controller = require('../common/api/weixin');

var router = express.Router();

router.post('/payconfig', controller.payconfig);
router.get('/paynotify', controller.paynotify);
router.get('/jssdk', controller.jssdk);
router.get('/token', controller.token);
router.get('/tickit/:type', controller.tickit);
router.get('/authurl', controller.authRedirect);
router.get('/auth', controller.auth);
router.use('/paynotify', controller.paynotify);



module.exports = router;
