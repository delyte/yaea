'use strict';

var express = require('express');
var controller = require('../common/api/page');
var auth = require('../common/auth/service');

var router = express.Router();

router.get('/layout', controller.layout);

module.exports = router;
