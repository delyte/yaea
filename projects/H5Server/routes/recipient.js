'use strict';

var express = require('express');
var controller = require('../common/api/recipient');
var auth = require('../common/auth/service');

var router = express.Router();

router.get('/defaulted', auth.isAuthenticated(), controller.defaulted);
router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);



module.exports = router;