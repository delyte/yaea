'use strict';

var express = require('express');
var controller = require('../common/api/product');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);

module.exports = router;
