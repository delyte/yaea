'use strict';

var express = require('express');
var controller = require('../common/api/order');
var auth = require('../common/auth/service');

var router = express.Router();

router.get('/catgory', auth.isAuthenticated(), controller.queryCatgory);
router.get('/catgorycount', auth.isAuthenticated(), controller.catgoryCount);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.put('/:id/next', auth.isAuthenticated(), controller.next);


module.exports = router;
