
'use strict';

angular.module('yaea.common.constants', [])

    .constant('appConfig', {
        userRoles: ['Admin', 'User'],
        codeTable: {
            List: {
                Type: [
                    { code: 'Activity', context: '活动' },
                    { code: 'Category', context: '分类' },
                    { code: 'Statistics', context: '自定义' }
                ],
                Status: [
                    { code: 'Online', context: '在线' },
                    { code: 'Offline', context: '下架' }
                ]
            },
            Supplier: {
                Type: [
                    { code: 'Normal', context: '普通' },
                    { code: 'High', context: '高级' }
                ],
                Status: [
                    { code: 'Online', context: '在线' },
                    { code: 'Offline', context: '下架' }
                ]
            },
            Page: {
                Layout: [
                    { code: 'Top', context: '头部' },
                    { code: 'Tag', context: '标签' },
                    { code: 'Body', context: '主体' }
                ]
            },
            Order: {
                Status: [
                    { code: 'Obligation', context: '待付款' },
                    { code: 'PendingForDeliver', context: '待发货' },
                    { code: 'Receiving', context: '待收货' },
                    { code: 'Evaluating', context: '待评价' },
                    { code: 'Finish', context: '已完成' }
                ]
            }
        }
    })
    .filter('displaySupplierType', ['appConfig', function (config) {
        return function (code) {
            var supplierType = _.find(config.codeTable.Supplier.Type, function (item) {
                return item.code == code;
            });
            return supplierType ? supplierType.context : '';
        }
    }])
    .filter('displaySupplierStatus', ['appConfig', function (config) {
        return function (code) {
            var supplierStatus = _.find(config.codeTable.Supplier.Status, function (item) {
                return item.code == code;
            });
            return supplierStatus ? supplierStatus.context : '';
        }
    }])
    .filter('displayListStatus', ['appConfig', function (config) {
        return function (code) {
            var listStatus = _.find(config.codeTable.List.Status, function (item) {
                return item.code == code;
            });
            return listStatus ? listStatus.context : '';
        }
    }])
    .filter('displayListType', ['appConfig', function (config) {
        return function (code) {
            var listType = _.find(config.codeTable.List.Type, function (item) {
                return item.code == code;
            });
            return listType ? listType.context : '';
        }
    }])
    .filter('displayOrderStatus', ['appConfig', function (config) {
        return function (code) {
            var listType = _.find(config.codeTable.Order.Status, function (item) {
                return item.code == code;
            });
            return listType ? listType.context : '';
        }
    }]);
