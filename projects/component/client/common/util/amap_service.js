angular.module('yaea.common.amap', [])
    .constant('AmapConfig', {
        amapKey: '0fe8ffcceb523ed289af03f5b0fb350b',
        districtUrl: 'http://restapi.amap.com/v3/config/district'
    })
    .service('AmapService', ['$http', 'AmapConfig', '$q', function ($http, config, $q) {
        var districtJSON = null;

        this.getDistrict = function () {

            return $q(function (resolve, reject) {
                if (districtJSON) {
                    resolve(districtJSON);
                    return;
                }
                var req = {
                    method: 'GET',
                    url: config.districtUrl,
                    params: {
                        'level': 'province',
                        'subdistrict': 2,
                        'key': config.amapKey
                    }
                };

                $http(req).then(function (respone) {
                    districtJSON = respone.data.districts;
                    resolve(districtJSON)
                }, reject)

            });
        }
    }]);

