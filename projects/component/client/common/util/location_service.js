angular.module('yaea.common.location', [])
    .constant('LocationConfig', {
        ingnorePath: ['/login']
    })

    .service('LocationService', ['$window', '$location', 'LocationConfig', function ($window, $location, config) {
        var pathHistory = [];

        this.path = function (url) {
            var path = $location.path();
            var index = _.indexOf(config.ingnorePath, path);
            if (index == -1) {
                pathHistory.push($location.path());
            }
            $location.path(url);
        }

        this.pathWithClear = function (url) {
            pathHistory = [];
            $location.path(url);
        }

        this.goBack = function () {
            var url = pathHistory.pop() || '/';
            $location.path(url);
        }

        this.goHome = function () {
            pathHistory = [];
            $location.path('/');
        }

    }]);