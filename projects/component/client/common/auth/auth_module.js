'use strict';

angular.module('yaea.auth', [
    'ngCookies',
    'yaea.common.constants',
    'yaea.common.service',
    'yaea.resource.user',
    'yaea.auth.service',
    'yaea.auth.interceptor',
    'yaea.common.location'

])
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
    });