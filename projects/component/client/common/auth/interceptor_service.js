'use strict';

angular.module('yaea.auth.interceptor', [])
    .factory('authInterceptor', [
        '$q',
        '$cookies',
        'Util',
        'LocationService',
        function authInterceptor($q, $cookies, Util, LocationService) {
            return {
                // Add authorization token to headers
                request: function(config) {
                    config.headers = config.headers || {};
                    if ($cookies.get('token') && Util.isSameOrigin(config.url)) {
                        config.headers.Authorization = 'Bearer ' + $cookies.get('token');
                    }
                    return config;
                },

                // Intercept 401s and redirect you to login
                responseError: function(response) {
                    if (response.status === 401) {
                        LocationService.path('/login');
                        // remove any stale tokens
                        $cookies.remove('token');
                    }
                    return $q.reject(response);
                }
            };
        }]);

