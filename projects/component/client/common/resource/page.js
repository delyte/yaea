angular.module('yaea.resource.page', ['ngResource'])
    .factory('Page', ['$resource', function ($resource) {
        return $resource('/api/page/:id', {
            id: '@_id'
        }, {
                update: { method: 'PUT' },
                showPage: {
                    method: 'GET',
                    params: {
                        id: 'type'
                    }
                },
                layout: {
                    method: 'GET',
                    params: {
                        id: 'layout'
                    }
                }
            });
    }]);