angular.module('yaea.resource.list', ['ngResource'])
    .factory('List', ['$resource', function($resource) {
        return $resource('/api/list/:id', {
            id: '@_id'
        }, {
                list: {
                    method: 'GET',
                    isArray: true,
                    params: {
                        id: 'list'
                    }
                },
                update: { method: 'PUT' },
                label: {
                    method: 'GET',
                    isArray: true,
                    params: {
                        id: 'labels'
                    }
                }
            });
    }]);