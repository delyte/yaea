angular.module('yaea.resource.order', ['ngResource'])
    .factory('Order', ['$resource', function ($resource) {
        return $resource('/api/order/:id/:controller', {
            id: '@_id'
        }, {
                update: { method: 'PUT' },
                catgory: {
                    method: 'GET',
                    params: {
                        id: 'catgory'
                    },
                    isArray: true,
                },
                catgoryCount: {
                    method: 'GET',
                    params: {
                        id: 'catgorycount'
                    },
                    isArray: true,
                },
                next: {
                    method: 'PUT',
                    params: {
                        controller: 'next'
                    }
                }
            });
    }]);