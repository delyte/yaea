angular.module('yaea.resource.recipient', ['ngResource'])
    .factory('Recipient', ['$resource', function ($resource) {
        return $resource('/api/recipient/:id', {
            id: '@_id'
        }, {
                update: { method: 'PUT' },
                defaulted: {
                    method: 'GET',
                    params: {
                        id: 'defaulted'
                    }
                },
            });
    }]);