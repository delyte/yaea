'use strict';

angular.module('yaea.resource.user',['ngResource'])
    .factory('User', ['$resource', function($resource) {
        return $resource('/api/user/:id/:controller', {
            id: '@_id'
        }, {
                changePassword: {
                    method: 'PUT',
                    params: {
                        controller: 'password'
                    }
                },
                get: {
                    method: 'GET',
                    params: {
                        id: 'me'
                    }
                },
                sendValidCode:{
                    method: 'POST',
                    params: {
                        id: 'validcode'
                    }
                }
            });
    }]);

