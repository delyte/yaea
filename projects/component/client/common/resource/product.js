angular.module('yaea.resource.product', ['ngResource'])
    .factory('Product', ['$resource', function ($resource) {
        return $resource('/api/product/:id', {
            id: '@_id'
        }, {
                update: { method: 'PUT' },
                labels: {
                    method: 'GET',
                    isArray: true,
                    params: {
                        id: 'labels'
                    }
                },
                getSearchSetup: {
                    method: 'GET',
                    isArray: true,
                    params: {
                        id: 'searchsetup'
                    }
                }
            });
    }]);