angular.module('yaea.resource.supplier', ['ngResource'])
    .factory('Supplier', ['$resource', function ($resource) {
        return $resource('/api/supplier/:id', {
            id: '@_id'
        }, {
            supplier: {
                method: 'GET',
                isArray: true,
                params: {
                    id: 'supplier'
                }
            }
            });
    }]);