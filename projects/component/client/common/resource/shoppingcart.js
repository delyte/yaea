angular.module('yaea.resource.shoppingcart', ['ngResource'])
    .factory('ShoppingCart', ['$resource', function ($resource) {
        return $resource('/api/shoppingcart/:id', {
            id: '@_id'
        }, {
                update: { method: 'PUT' }
            });
    }]);