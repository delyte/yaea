var gulp = require('gulp'),
     path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath),
    replace = require('gulp-replace');
exports.task = function() {
    var inject = [];
    if (config.cordova) {
        inject.push('<script src="cordova.js"></script>');
    }
    inject.push('<script src="/js/app.min.js"></script>');
    gulp.src(['src/html/**/*.html'])
        .pipe(replace('<!-- inject:js -->', inject.join('\n    ')))
        .pipe(gulp.dest(config.dest));
}