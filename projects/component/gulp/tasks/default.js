var seq = require('run-sequence'),
     path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath);

exports.task = function(done) {
    var tasks = [];

    if (typeof config.server === 'object') {
        tasks.push('connect');
    }

    tasks.push('watch');

    seq('build', tasks, done);
}