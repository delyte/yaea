var seq = require('run-sequence');
exports.task = function(done) {
    var tasks = ['html', 'fonts', 'images', 'less', 'js'];
    seq('clean', tasks, done);
};
