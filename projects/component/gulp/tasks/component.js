var gulp = require('gulp'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    babel = require('gulp-babel');
exports.task = function() {

    return gulp.src(['./component/server/**/*.js'])
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('./bin/manager/common'))
        .pipe(gulp.dest('./bin/H5Server/common'))
        .pipe(gulp.dest('./bin/weixinServer/common'));


};