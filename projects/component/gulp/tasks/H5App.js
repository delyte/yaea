var gulp = require('gulp'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    streamqueue = require('streamqueue'),
    babel = require('gulp-babel');
exports.task = function() {

    return gulp.src('./H5App/www/**/*')
        .pipe(gulp.dest('./bin/H5App/www'))
};