var gulp = require('gulp'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    streamqueue = require('streamqueue'),
    babel = require('gulp-babel');
exports.dependencies = ['H5App'];
exports.task = function() {
    return streamqueue({ objectMode: true },
        gulp.src('./H5Server/package.json'),
        gulp.src(['./H5Server/**/*.js', '!./H5Server/node_modules/**/*'])
            .pipe(babel({
                presets: ['es2015']
            }))

    )
        .pipe(gulp.dest('./bin/H5Server'));
};