var gulp = require('gulp'),
     path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath);
exports.task = function() {
    return gulp.src(config.vendor.fonts)
        .pipe(gulp.dest(path.join(config.dest, 'fonts')));
};