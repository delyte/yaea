var gulp = require('gulp'),
     path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath);
exports.task = function() {
    return gulp.src('src/images/**/*')
        .pipe(gulp.dest(path.join(config.dest, 'images')));
};