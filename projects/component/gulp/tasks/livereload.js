var gulp = require('gulp'),
     path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath),
    connect = require('gulp-connect');

exports.task = function() {
    gulp.src(path.join(config.dest, '*.html'))
        .pipe(connect.reload());
};