var gulp = require('gulp'),
    path = require('path'),
    streamqueue = require('streamqueue'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath),
    ngFilesort = require('gulp-angular-filesort'),
    templateCache = require('gulp-angular-templatecache'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');
exports.task = function() {
    streamqueue({ objectMode: true },
        gulp.src(config.vendor.js),
        gulp.src('./src/commonjs/**/*.js').pipe(ngFilesort()),
        gulp.src('./src/js/**/*.js').pipe(ngFilesort()),
        gulp.src(['src/templates/**/*.html', 'src/js/directives/templates/*.html']).pipe(templateCache({ module: config.appNanme }))
    )
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.join(config.dest, 'js')));
};