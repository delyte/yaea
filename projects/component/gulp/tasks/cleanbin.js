var gulp = require('gulp'),
    path = require('path'),
    rimraf = require('gulp-rimraf');
exports.task = function(cb) {
    return gulp.src('./bin', { read: false })
        .pipe(rimraf());
};