var gulp = require('gulp'),
    path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath),
    mobilizer = require('gulp-mobilizer'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    less = require('gulp-less');
exports.task = function () {
    return gulp.src(config.less.src).pipe(less({
        paths: config.less.paths.map(function (p) {
            return p;
        })
    }))
        .pipe(mobilizer('app.css', {
            'app.css': {
                hover: 'exclude',
                screens: ['0px']
            },
            'hover.css': {
                hover: 'only',
                screens: ['0px']
            }
        }))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(path.join(config.dest, 'css')));
}