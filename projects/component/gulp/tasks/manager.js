var gulp = require('gulp'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    streamqueue = require('streamqueue'),
    babel = require('gulp-babel');
exports.dependencies = ['managerApp'];
exports.task = function() {
   return  streamqueue({ objectMode: true },
        gulp.src('./manager/package.json'),
        gulp.src(['./manager/**/*.js', '!./manager/node_modules/**/*'])
            .pipe(babel({
                presets: ['es2015']
            }))

    )
        .pipe(gulp.dest('./bin/manager'));
};