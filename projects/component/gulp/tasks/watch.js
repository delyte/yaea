var gulp = require('gulp'),
    path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath);
exports.task = function() {
    if (typeof config.server === 'object') {
        gulp.watch([config.dest + '/**/*'], ['livereload']);
    }
    gulp.watch(['./src/html/**/*'], ['html']);
    gulp.watch(['./src/less/**/*'], ['less']);
    gulp.watch(['./src/commonjs/**/*', './src/js/**/*', './src/templates/**/*', config.vendor.js], ['js']);
    gulp.watch(['./src/images/**/*'], ['images']);
};