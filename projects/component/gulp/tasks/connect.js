var gulp = require('gulp'),
    path = require('path'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath);
connect = require('gulp-connect');


exports.task = function() {
    if (typeof config.server === 'object') {
        connect.server({
            root: config.dest,
            host: config.server.host,
            port: config.server.port,
            livereload: true
        });
    } else {
        throw new Error('Connect is not configured');
    }
}