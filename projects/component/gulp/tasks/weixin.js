var gulp = require('gulp'),
    path = require('path'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    streamqueue = require('streamqueue'),
    babel = require('gulp-babel');
exports.task = function() {
    return streamqueue({ objectMode: true },
        gulp.src('./weixinServer/package.json'),
        gulp.src(['./weixinServer/**/*.js', '!./weixinServer/node_modules/**/*'])
            .pipe(babel({
                presets: ['es2015']
            }))

    )
        .pipe(gulp.dest('./bin/weixinServer'));
};