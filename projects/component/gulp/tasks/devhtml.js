var gulp = require('gulp'),
    path = require('path'),
    streamqueue = require('streamqueue'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath),
    ngFilesort = require('gulp-angular-filesort'),
    templateCache = require('gulp-angular-templatecache'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    ngAnnotate = require('gulp-ng-annotate'),
    flatten = require('gulp-flatten'),
    uglify = require('gulp-uglify'),
    inject = require('gulp-inject'),
    rename = require('gulp-rename');
exports.task = function() {
    var jssource = streamqueue({ objectMode: true },
        gulp.src(config.vendor.js),
        gulp.src('./src/commonjs/**/*.js').pipe(ngFilesort()),
        gulp.src('./src/js/**/*.js').pipe(ngFilesort()),
        gulp.src(['src/templates/**/*.html', 'src/js/directives/templates/*.html']).pipe(templateCache({ module: config.appNanme }))
    )

        .pipe(gulp.dest(path.join(config.dest, 'js')));


    gulp.src(['src/html/**/*.html'])
        .pipe(inject(jssource, {
            transform: function(filepath) {
                var fileName = filepath.replace('/www', '');
                if (fileName.lastIndexOf('.js') > 0) {
                    return '<script src="' + fileName + '"></script>';
                } else {
                    console.log('html task support js/css injection only.');
                }
            }
        }))
        .pipe(gulp.dest(config.dest));
};