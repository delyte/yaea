var seq = require('run-sequence');
exports.task = function(done) {
    var tasks = ['devhtml', 'fonts', 'images', 'less'];
    seq('clean', tasks, done);
};