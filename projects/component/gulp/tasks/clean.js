var gulp = require('gulp'),
    path = require('path'),
    rimraf = require('gulp-rimraf'),
    currentParentPath = process.cwd(),
    configPath = path.resolve(currentParentPath, 'gulp/gulpconfig'),
    config = require(configPath);
exports.task = function(cb) {
    return gulp.src([
        path.join(config.dest, 'index.html'),
        path.join(config.dest, 'images'),
        path.join(config.dest, 'css'),
        path.join(config.dest, 'js'),
        path.join(config.dest, 'fonts')
    ], { read: false })
        .pipe(rimraf());
};