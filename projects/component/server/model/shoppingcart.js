var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var codeTable = require('./CodeTable');
var Schema = mongoose.Schema;


var ShoppingCartSchema = new Schema({
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
    cart: [{
        product: {
            type: mongoose.Schema.ObjectId,
            ref: 'Product'
        },
        count: Number
    }]
});

module.exports = mongoose.model('ShoppingCart', ShoppingCartSchema);