
module.exports = {
    List: {
        Type: ['Activity', 'Category', 'Statistics'],
        Status: ['Online', 'Offline']
    },
    Supplier: {
        Type: ['Normal', 'High'],
        Status: ['Online', 'Offline']
    },
    Order: {
        Status: ['Obligation', 'PendingForDeliver', 'Receiving', 'Evaluating', 'Finish']
    },
    User: {
        Role: ['Admin', 'User']
    },
    Page: {
        Type: ['Main'],
        Layout: ['Top', 'Tag', 'Body']
    }

}