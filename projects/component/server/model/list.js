var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var codeTable = require('./CodeTable');
var utility = require('../api/utility');
var Schema = mongoose.Schema;

var ListSchema = new Schema({
  name: String,
  sn: String,
  type: {
    type: String,
    enum: codeTable.List.Type
  },
  status: {
    type: String,
    enum: codeTable.List.Status
  },
  startTime: Date,
  endTime: Date,
  content: String,
  description: String,
  cover: [String],
  attachments: [String],
  labels: [{
    text: String,
  }],
  productRelation: String,
  searchText: String,
  createdTime: { type: Date, default: Date.now() }
}, {
    toObject: { virtuals: true, getters: true },
    toJSON: { virtuals: true, getters: true }
  });

ListSchema.index({
  'name': 'text',
  'sn': 'text',
  'labels.text': 'text',
  'description': 'text',
  'searchText': 'text',
}, { name: 'list_schema_index' });

var nodejieba = require("nodejieba");

var getSearchText = function (list) {
  var searchText = '';
  searchText += list.sn + " ";
  searchText += nodejieba.cut(list.name).join(' ') + " ";
  searchText += nodejieba.cut(list.description).join(' ') + " ";

  list.labels.forEach(function (label) {
    searchText += nodejieba.cut(label.text).join(' ') + " ";
  });
  console.log("searchText", searchText);
  return searchText;
};


ListSchema.statics.updateSearchText = function (id, cb) {
  this.findOne({ _id: id }).exec(function (err, product) {
    if (err) { if (cb) { cb(err); } return; }
    var searchText = getSearchText(product);
    this.update({ _id: id }, { searchText: searchText }, function (err, num) {
      if (cb) { cb(err); }
    });
  }.bind(this));
};



ListSchema.pre('save', function (next) {
  if(!this.sn){
     this.sn = utility.generateNumber('YAEALST-');
  }
 
  this.searchText = getSearchText(this);
  next();
});




module.exports = mongoose.model('List', ListSchema);