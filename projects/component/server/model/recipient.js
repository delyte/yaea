var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var codeTable = require('./CodeTable');
var Schema = mongoose.Schema;


var RecipientSchema = new Schema({
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
    name: String,
    phone: String,
    address: String,
    post: String,
    defaulted: Boolean
});

module.exports = mongoose.model('Recipient', RecipientSchema);