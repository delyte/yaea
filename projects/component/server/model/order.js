var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var codeTable = require('./CodeTable');
var Schema = mongoose.Schema;
var utility = require('../api/utility');


var OrderSchema = new Schema({
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
    sn: String,
    status: {
        type: String,
        enum: codeTable.Order.Status,
        default: 'Obligation'
    },
    orderedOn: {
        type: Date, default: Date.now()
    },
    recipient: {
        type: mongoose.Schema.ObjectId,
        ref: 'Recipient'
    },
    orders: [{
        product: {
            type: mongoose.Schema.ObjectId,
            ref: 'Product'
        },
        count: Number
    }],
    bill: {
        productBill: Number,
        carriageBill: Number
    },
    remark: String
});

OrderSchema.pre('save', function (next) {
    if (!this.sn) {
        this.sn = utility.generateNumber('YAEAORDER-');
    }
    next();
});


module.exports = mongoose.model('Order', OrderSchema);

