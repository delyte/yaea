var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var Schema = mongoose.Schema;
var utility = require('../api/utility');

var ProductSchema = new Schema({
  name: String,
  sn: String,
  price: { type: Number, default: 0 },
  inStock: { type: Number, default: 0 },
  sold: { type: Number, default: 0 },
  origin: String,
  onlineTime: Date,
  supplier: {
    id: {
      type: Schema.Types.ObjectId, ref: 'Supplier'
    },
    name: String
  },
  labels: [{
    text: String,
  }],
  title: String,
  description: String,
  cover: [String],
  images: [String],
  searchText: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  comments: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      },
      stars: {
        type: Number,
        default: 0
      },
      Approval: {
        type: Number,
        default: 0
      },
      content: String,
      createdAt: {
        type: Date,
        default: Date.now
      },
      answers: [
        {
          user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
          },
          content: String,
          createdAt: {
            type: Date,
            default: Date.now
          }
        }
      ]
    }
  ]
}, {
    toObject: { virtuals: true, getters: true },
    toJSON: { virtuals: true, getters: true }
  });


ProductSchema.index({
  'name': 'text',
  'sn': 'text',
  'origin': 'text',
  'labels.text': 'text',
  'searchText': 'text',
}, { name: 'product_schema_index' });


var nodejieba = require("nodejieba");

var getSearchText = function (product) {
  var searchText = '';
  searchText += product.sn + " ";
  searchText += nodejieba.cut(product.name).join(' ') + " ";
  searchText += nodejieba.cut(product.origin).join(' ') + " ";
  searchText += nodejieba.cut(product.supplier.name).join(' ') + " ";

  product.labels.forEach(function (label) {
    searchText += nodejieba.cut(label.text).join(' ') + " ";
  });
  console.log("searchText", searchText);
  return searchText;
};

ProductSchema.statics.updateSearchText = function (id, cb) {
  this.findOne({ _id: id }).exec(function (err, product) {
    if (err) { if (cb) { cb(err); } return; }
    var searchText = getSearchText(product);
    this.update({ _id: id }, { searchText: searchText }, function (err, num) {
      if (cb) { cb(err); }
    });
  }.bind(this));
};



ProductSchema.pre('save', function (next) {
  if (!this.sn) {
    this.sn = utility.generateNumber('YAEAPRD-');
  }
  this.searchText = getSearchText(this);
  next();
});

module.exports = mongoose.model('Product', ProductSchema);