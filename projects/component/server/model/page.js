var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var codeTable = require('./codetable');
var list = require('./list');
var utility = require('../api/utility');


var Schema = mongoose.Schema;

var PageSchema = new Schema({
  type: {
    type: String,
    enum: codeTable.Page.Type
  },
  layout: [{
    type: { type: String, enum: codeTable.Page.Layout },
    lists: [{
      id: { type: Schema.Types.ObjectId, ref: 'List' },
      name: String
    }]
  }]
}, {
    toObject: { virtuals: true, getters: true },
    toJSON: { virtuals: true, getters: true }
  });





module.exports = mongoose.model('Page', PageSchema);
