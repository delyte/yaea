var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var codeTable = require('./codetable');
var utility = require('../api/utility');

var Schema = mongoose.Schema;

var SupplierSchema = new Schema({
  name: String,
  sn: String,
  type: {
    type: String,
    enum: codeTable.Supplier.Type
  },
  status: {
    type: String,
    enum: codeTable.Supplier.Status
  },
  contact: String,
  phone: String,
  address: String,
  post: String,
  description: String,
  cover: [String],
  attachments: [String],
  createdTime: {
    type: Date, default: Date.now()
  },
  searchText: String
}, {
  toObject: { virtuals: true, getters: true },
    toJSON: { virtuals: true, getters: true }
  });


SupplierSchema.index({
  'name': 'text',
  'sn': 'text',
  'contact': 'text',
  'phone': 'text',
  'address': 'text',
  'description': 'text',
  'searchText': 'text',
}, { name: 'supplier_schema_index' });

var nodejieba = require("nodejieba");

var getSearchText = function (supplier) {
  var searchText = "";
  searchText += supplier.sn + " ";
  searchText += nodejieba.cut(supplier.name).join(' ') + " ";
  searchText += nodejieba.cut(supplier.address).join(' ') + " ";
   
  console.log("searchText", searchText);
  return searchText;
};

SupplierSchema.statics.updateSearchText = function(id, cb){
  this.findOne({_id: id}).exec(function(err, supplier){
    if(err){ if(cb){cb(err);} return; }
    var searchText = getSearchText(supplier);
    this.update({_id: id}, {searchText: searchText}, function(err, num){
      if(cb){cb(err);}
    });
  }.bind(this));
};


SupplierSchema.pre('save', function(next){
  this.sn = utility.generateNumber('YAEASPL-');
  this.searchText = getSearchText(this);
  next();
});

export default mongoose.model('Supplier', SupplierSchema);
