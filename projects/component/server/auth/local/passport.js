var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

function localAuthenticate(User, phonenumber, password, done) {
    User.findOneAsync({
        $or: [{ 'phonenumber': phonenumber }, { 'name': phonenumber }, { 'email': phonenumber }]
    })
        .then(function(user) {
            if (!user) {
                return done(null, false, {
                    message: '用户不存在！'
                });
            }
            user.authenticate(password, function(authError, authenticated) {
                if (authError) {
                    return done(authError);
                }
                if (!authenticated) {
                    return done(null, false, { message: '密码错误！' });
                } else {
                    return done(null, user);
                }
            });
        })
        .catch(function(err) {
            console.log(err);
            done(err)
        });
}

exports.setup = function(User) {
    passport.use(new LocalStrategy({
        usernameField: 'phonenumber',
        passwordField: 'password' // this is the virtual field on the model
    }, function(phonenumber, password, done) {
        return localAuthenticate(User, phonenumber, password, done);
    }));
}
