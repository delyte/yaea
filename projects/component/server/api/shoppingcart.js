var ShoppingCart = require('../model/shoppingcart');
var Handler = require('./handler');
var Promise = require('bluebird');
require('../model/product');


// Gets a single list from the DB
module.exports.show = function (req, res) {
    var userid = req.user.id;

    ShoppingCart.findOne({ user: userid }).populate('cart.product', 'name price title cover inStock').execAsync()
        .then(respondWithResult(res))
        .catch(Handler.handleError(res));
}


var respondWithResult = function (res, statusCode) {
    statusCode = statusCode || 200;
    return function (entity) {
        if (!entity) {
            entity = {};
        }
        res.status(statusCode).json(entity);
    };
}



module.exports.create = function (req, res) {
    var userid = req.user.id;
    req.body.user = userid;
    ShoppingCart.createAsync(req.body)
        .then(showCart)
        .then(Handler.respondWithResult(res, 201))
        .catch(Handler.handleError(res));
}

function showCart(entity) {
    return ShoppingCart.findOne({ user: entity.user }).populate('cart.product', 'name price title cover inStock').execAsync()
}

module.exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    var userid = req.user.id;
    req.body.user = userid;
    ShoppingCart.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.saveUpdates(req.body))
        .then(showCart)
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


var handleCartNotFound = function (entity) {
    if (!entity) {
        return {};
    }
    return entity;
}
