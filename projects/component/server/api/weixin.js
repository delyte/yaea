var Weixin = require('../external/weixin');
var Handler = require('./handler');
var util = require('util');

module.exports.token = function (req, res) {
    Weixin.token.get()
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


module.exports.tickit = function (req, res) {
    var type = req.params.type;
    Weixin.tickit.get(type)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


module.exports.jssdk = function (req, res) {
    var pageUrl = req.query.pageUrl;
    Weixin.jssdk.config(pageUrl)
        .then(Handler.handleEntityNotFound(res))
        .then(respondConfigWithResult(req, res))
        .catch(Handler.handleError(res));
}

var respondConfigWithResult = function (req, res, statusCode) {
    statusCode = statusCode || 200;
    return function (entity) {
        if (entity) {
            req.session.weixinJsSdkConfig = entity;
            res.status(statusCode).json(entity);
        }
    };
}


module.exports.payconfig = function (req, res) {
    var body = req.body;
    //req.session.weixinAuth.openid = 'oRWzPv6KC-QlKcKFLyw_-XkshzvA';
    /* if (util.isNullOrUndefined(req.session.weixinAuth) || util.isNullOrUndefined(req.session.weixinAuth.openid)) {
         Handler.handleError(res)('请使用微信登陆');
         return;
     }*/
    var openid = 'oRWzPv5ofKiHHTA3OVxAVe9W6QtU';//req.session.weixinAuth.openid
    Weixin.pay.payConfig(body.description, body.orderNumber, body.totalFee, openid, req)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


module.exports.paynotify = function (req, res) {
    console.log(req.query);
    console.log(req.body);
}


module.exports.authRedirect = function (req, res) {
    var query = req.query;
    var authUrl = Weixin.authorize.getAuthorizeURL(query.redirect, query.state);
    console.log(authUrl);
    res.redirect(authUrl);
}


module.exports.auth = function (req, res) {
    if (util.isNullOrUndefined(req.session.weixinAuth)) {
        req.session.weixinAuth = {};
    }
    var query = req.query;
    Weixin.authorize.processAuth(query.code, query.state).then(function (data) {
        req.session.weixinAuth = data;
        return { isAuth: true };
    })
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

