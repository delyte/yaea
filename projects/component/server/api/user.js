'use strict';

var User = require('../model/user');
var jwt = require('jsonwebtoken');
var Handler = require('./handler');
var config = require('../config');
var Promise = require('bluebird');
var utility = require('./utility');
require('../extension/date');

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {
    User.findAsync({}, '-password -salt')
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


function isExistPhoneNumber(phoneNumber) {
    return new Promise(function (resolve, reject) {
        User.countAsync({ phonenumber: phoneNumber }).then(function (ct) {
            if (ct > 0) {
                reject({ msg: '号码已存在' });
            }
            else {
                resolve(phoneNumber);
            }
        }).catch(reject);
    });
}


function checkValidCodeExpired(req, res) {
    return function (phoneNumber) {
        return new Promise(function (resolve, reject) {
            var validCodeInfo = req.session.validCodes && req.session.validCodes[phoneNumber];
            if (validCodeInfo) {
                var sendTime = validCodeInfo.sendTime;
                if (sendTime < (new Date()).addMinutes(1)) {
                    reject({ msg: '该号码刚刚获取过验证码，请耐心等待一分钟！' })
                }
                else {
                    resolve(phoneNumber);
                }
            }
            else {
                resolve(phoneNumber);
            }
        });
    }

}

function handleValidCodeSuccess(req, res, statusCode) {
    statusCode = statusCode || 200;
    return function (data) {
        if (!req.session.validCodes) {
            req.session.validCodes = {};
        }
        req.session.validCodes[data.phoneNumber] = data;
        res.status(statusCode).json({ success: true });
    }

}



exports.sendValidCode = function (req, res) {
    var phoneNumber = String(req.body.phoneNumber);
    isExistPhoneNumber(phoneNumber)
        .then(checkValidCodeExpired(req, res))
        .then(utility.sendValidCode)
        .then(handleValidCodeSuccess(req, res))
        .catch(Handler.handleError(res));

}

function verifyValidCode(req, res) {
    return new Promise(function (resolve, reject) {
        var phoneNumber = req.body.phonenumber,
            validCode = req.body.validCode;
        var validCodeInfo = req.session.validCodes && req.session.validCodes[phoneNumber];
        if (validCodeInfo) {
            var code = validCodeInfo.code,
                expiredTime = validCodeInfo.expiredTime;
            if (expiredTime < new Date()) {
                res.status(442).json({ msg: '验证码已过期！' });
                resolve(false);
                return;
            }
            else if (validCode != code) {
                res.status(442).json({ msg: '验证码不正确！' });
                resolve(false);
                return;
            }

        }
        else {
            res.status(442).json({ msg: '验证码不正确！' });
            resolve(false);
            return;
        }
        resolve(true);


    });
}


/**
 * Creates a new user
 */
exports.createForApp = function (req, res, next) {
    verifyValidCode(req, res).then(function (isTrue) {
        if (isTrue == false) {
            return;
        }
        if (req.body.validCode) {
            delete req.body.validCode;
        }
        var newUser = new User(req.body);
        newUser.saveAsync()
            .spread(function (user) {
                var token = jwt.sign({ _id: user._id }, config.user_jwt.secrets, {
                    expiresIn: 60 * 60 * 5
                });
                res.json({ token });
            })
            .catch(Handler.validationError(res));
    })

}

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {

    var newUser = new User(req.body);
    newUser.saveAsync()
        .spread(function (user) {
            var token = jwt.sign({ _id: user._id }, config.user_jwt.secrets, {
                expiresIn: 60 * 60 * 5
            });
            res.json({ token });
        })
        .catch(Handler.validationError(res));
}


/**
 * Get a single user
 */
exports.show = function (req, res, next) {
    var userId = req.params.id;

    User.findByIdAsync(userId)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
    User.findByIdAndRemoveAsync(req.params.id)
        .then(function () {
            res.status(204).end();
        })
        .catch(Handler.handleError(res));
}

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
    var userId = req.user._id;
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);

    User.findByIdAsync(userId)
        .then(function (user) {
            if (user.authenticate(oldPass)) {
                user.password = newPass;
                return user.saveAsync()
                    .then(function () {
                        res.status(204).end();
                    })
                    .catch(Handler.validationError(res));
            } else {
                return res.status(403).end();
            }
        });
}

/**
 * Get my info
 */
exports.me = function (req, res, next) {
    var userId = req.user._id;

    User.findOneAsync({ _id: userId }, '-password -salt')
        .then(function (user) { // don't ever give out the password or salt
            if (!user) {
                return res.status(401).end();
            }
            res.json(user);
        })
        .catch(function (err) { next(err) });
}



/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
    res.redirect('/');
}
