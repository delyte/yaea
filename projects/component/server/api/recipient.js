var Recipient = require('../model/recipient');
var Handler = require('./handler');
var Promise = require('bluebird');
var _ = require('lodash');


// Gets a single list from the DB
module.exports.index = function (req, res) {
    var userid = req.user.id;
    Recipient.findAsync({ user: userid })
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.show = function (req, res) {
    Recipient.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.create = function (req, res) {
    var userid = req.user.id;
    req.body.user = userid;
    updateDefualted(req.body).then(function () {
        Recipient.createAsync(req.body)
            .then(Handler.respondWithResult(res, 201))
            .catch(Handler.handleError(res));
    }).catch(Handler.handleError(res));

}

module.exports.defaulted = function (req, res) {
    var userid = req.user.id;
    Recipient.findOneAsync({ user: userid, defaulted: true })
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));

}



module.exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    var userid = req.user.id;
    req.body.user = userid;
    updateDefualted(req.body).then(function () {
        Recipient.findByIdAsync(req.params.id)
            .then(Handler.handleEntityNotFound(res))
            .then(Handler.saveUpdates(req.body))
            .then(Handler.respondWithResult(res))
            .catch(Handler.handleError(res));
    }).catch(Handler.handleError(res));

}

module.exports.destroy = function (req, res) {
    Recipient.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.removeEntity(res))
        .catch(Handler.handleError(res));
}

function updateDefualted(body) {
    return new Promise(function (resolve, reject) {
        if (body.defaulted) {
            Recipient.updateAsync({ user: body.user }, { $set: { defaulted: false } }, { multi: true }).then(resolve, reject);
        }
        else {
            resolve(true);
        }
    });

}
