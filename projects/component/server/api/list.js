var List = require('../model/list');
var Handler = require('./handler');
var Promise = require('bluebird');
var _ = require('lodash');


// Gets a single list from the DB
module.exports.index = function(req, res) {
    var pageindex = req.query.pageindex,
        skipCount = (pageindex - 1) * 10;
    var query = req.query.query && JSON.parse(req.query.query);
    Promise.all([
        List.find(query).sort({ createdAt: -1 }).skip(skipCount).limit(10).execAsync(),
        List.countAsync(query)
    ])
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


module.exports.list = function(req, res) {

    List.find().select('id name').execAsync()
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.show = function(req, res) {
    List.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.create = function(req, res) {
    List.createAsync(req.body)
        .then(Handler.respondWithResult(res, 201))
        .catch(Handler.handleError(res));
}

module.exports.update = function(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    List.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.saveUpdates(req.body))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.destroy = function(req, res) {
    List.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.removeEntity(res))
        .catch(Handler.handleError(res));
}

module.exports.labels = function (req, res) {
    List.distinctAsync('labels.text')
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}