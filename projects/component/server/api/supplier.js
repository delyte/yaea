var Handler = require('./handler')
import Supplier from '../model/supplier';


// Gets a list of suppliers
export function index(req, res) {
    var query = req.query.query && JSON.parse(req.query.query);
    var pageindex = req.query.pageindex,
        skipCount = (pageindex - 1) * 10;
    var query = req.query.query && JSON.parse(req.query.query);

    Promise.all([
        Supplier.find(query).sort({ createdTime: -1 }).skip(skipCount).limit(10).execAsync(),
        Supplier.countAsync(query)
    ]).then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

// Gets a single supplier from the DB
export function show(req, res) {
    Supplier.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

// Creates a new supplier in the DB
export function create(req, res) {
    Supplier.createAsync(req.body)
        .then(Handler.respondWithResult(res, 201))
        .catch(Handler.handleError(res));
}

// Updates an existing supplier in the DB
export function update(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Supplier.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.saveUpdates(req.body))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

// Deletes a supplier from the DB
export function destroy(req, res) {
    Supplier.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.removeEntity(res))
        .catch(Handler.handleError(res));
}

export function supplier(req, res) {
    Supplier.find().select('id name').execAsync()
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}
