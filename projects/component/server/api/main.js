var Product = require('../model/product');
var Handler = require('./handler')

// Gets a single product from the DB
module.export.show = function (req, res) {
    Product.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}