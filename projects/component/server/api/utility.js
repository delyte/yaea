
var fs = require('bluebird').promisifyAll(require("fs"));
var path = require('path');
var Handler = require('./handler');
var messager = require('../external/alidayu/messager');
var Promise = require('bluebird');
var config = require('../config');
require('../extension/date');



module.exports.upload = function (req, res) {
    res.json({ fileName: req.file.filename });
}

var createTimestamp = function () {
    return parseInt(new Date().getTime() / 1000) + '';
};

module.exports.generateNumber = function (prefix) {
    return prefix + createTimestamp();
}

module.exports.photoGalleryNames = function (folderName) {
    return function (req, res) {
        fs.readdirAsync(folderName)
            .then(Handler.respondWithResult(res))
            .catch(Handler.handleError(res));
    }
}


module.exports.sendValidCode = function (phoneNumber) {

    return new Promise(function (resolve, reject) {

        var validCode = getRandom(4);
        messager.send('注册验证', 'SMS_5054317', phoneNumber, { "code": validCode, "product": config.productName })
            .then(function (data) {
                if (data.result.success == true) {
                    resolve({
                        phoneNumber: phoneNumber,
                        code: validCode,
                        sendTime: new Date(),
                        expiredTime: (new Date()).addMinutes(2)
                    });
                }
                else {
                    reject({ msg: '发送短信异常，请等待一分钟后再发' });
                }
            })
            .catch(reject);
    });


}



module.exports.generateNonceString = function (length) {
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var maxPos = chars.length;
    var noceStr = "";
    for (var i = 0; i < (length || 32); i++) {
        noceStr += chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return noceStr;
};

module.exports.createTimestamp = function () {
    return parseInt(new Date().getTime() / 1000).toString();
};

module.exports.getIPAdress = function () {
    var interfaces = require('os').networkInterfaces();
    for (var devName in interfaces) {
        var iface = interfaces[devName];
        for (var i = 0; i < iface.length; i++) {
            var alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                return alias.address;
            }
        }
    }
}

module.exports.createBillNo = function (mchId) {
    return mchId + createTimestamp();
};



var getRandom = function (bitNum) {
    var jschars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

    var res = "";
    for (var i = 0; i < bitNum; i++) {
        var id = Math.ceil(Math.random() * 9);
        res += jschars[id];
    }
    return res;
}
