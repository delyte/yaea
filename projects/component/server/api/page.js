var Page = require('../model/page');
var Product = require('../model/product');
var Handler = require('./handler');
var Promise = require('bluebird');
var _ = require('lodash');



module.exports.show = function (req, res) {
    Page.findOneAsync({ type: req.query.type })
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


var adjustPageInfo = function (res, statusCode) {
    statusCode = statusCode || 200;
    return function (entity) {
        if (entity) {
            var layout = entity.layout;
            var layoutResult = {};
            layout.forEach(function (ly) {
                layoutResult[ly.type] = [];
                ly.lists.forEach(function (list) {
                    layoutResult[ly.type].push(list.id);
                })

            });
            addProductsIntoBody(layoutResult).then(
                function (data) {
                    res.status(statusCode).json(data);
                }
            )

        }
    };
};

var addProductsIntoBody = function (layoutResult) {

    return new Promise(function (reslove, reject) {
        var relationArray = [];
        var bodyLists = layoutResult['Body'];
        bodyLists.forEach(function (list) {
            var relation = list.productRelation && JSON.parse(list.productRelation);
            relationArray.push(Product.find(relation).select('id name cover').limit(8).execAsync());
        });

        Promise.all(relationArray)
            .then(function (collection) {
                for (var index in collection) {
                    bodyLists[index]._doc.products = collection[index];
                }
                layoutResult['Body'] = bodyLists;
                reslove(layoutResult);
            }).catch(function (err) {
                reject(err);
            })

    });



}



module.exports.layout = function (req, res) {
    Page.findOne({ type: req.query.type }).populate('layout.lists.id').execAsync()
        .then(adjustPageInfo(res))
        .catch(Handler.handleError(res));
}


module.exports.create = function (req, res) {
    Page.createAsync(req.body)
        .then(Handler.respondWithResult(res, 201))
        .catch(Handler.handleError(res));
}

module.exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Page.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.saveUpdates(req.body))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


