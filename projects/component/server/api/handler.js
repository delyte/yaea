'use strict';

var _ = require('lodash');

var handler = {};

handler.respondWithResult = function (res, statusCode) {
    statusCode = statusCode || 200;
    return function (entity) {
        if (entity) {
            res.status(statusCode).json(entity);
        }
    };
}

handler.saveUpdates = function (updates) {
    return function (entity) {
        var updated = _.extend(entity, updates);
        return updated.saveAsync()
            .spread(updated => {
                return updated;
            });
    };
}

handler.removeEntity = function (res) {
    return function (entity) {
        if (entity) {
            return entity.removeAsync()
                .then(() => {
                    res.status(204).end();
                });
        }
    };
}

handler.handleEntityNotFound = function (res) {
    return function (entity) {
        if (!entity) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
}

handler.handleError = function (res, statusCode) {
    statusCode = statusCode || 500;
    return function (err) {
        res.status(statusCode).send(err);
    };
}

handler.handleUnauthorized = function (req, res) {
    return function (entity) {
        if (!entity) { return null; }
        if (entity.user._id.toString() !== req.user._id.toString()) {
            res.send(403).end();
            return null;
        }
        return entity;
    }
}

handler.validationError = function (res, statusCode) {
    statusCode = statusCode || 422;
    return function (err) {
        res.status(statusCode).json(err);
    }
}

module.exports = handler;