var Product = require('../model/product');
var Handler = require('./handler');
var Promise = require('bluebird');
var _ = require('lodash');
var nodejieba = require("nodejieba");



// Gets a single product from the DB
module.exports.index = function (req, res) {
    var pageindex = req.query.pageindex,
        skipCount = (pageindex - 1) * 10;
    var query = req.query.query && JSON.parse(req.query.query);
    if (query && query.$text) {
        var searchText = nodejieba.cut(query.$text.$search).join(' ');
        query.$text.$search = searchText;
    }
    Promise.all([
        Product.find(query).sort({ createdAt: -1 }).skip(skipCount).limit(10).execAsync(),
        Product.countAsync(query)
    ])
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.show = function (req, res) {
    Product.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.create = function (req, res) {
    Product.createAsync(req.body)
        .then(Handler.respondWithResult(res, 201))
        .catch(Handler.handleError(res));
}

module.exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Product.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.saveUpdates(req.body))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.destroy = function (req, res) {
    Product.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.removeEntity(res))
        .catch(Handler.handleError(res));
}



module.exports.labels = function (req, res) {
    Product.distinctAsync('labels.text')
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


module.exports.searchsetup = function (req, res) {
    Promise.all([
        Product.distinctAsync('labels.text'),
        Product.distinctAsync('origin')
    ]).then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}




module.exports.createComments = function (req, res) {
    req.body.user = req.user._id;
    Product.update({ _id: req.params.id }, { $push: { comments: req.body } }, function (err, num) {
        if (err) { return Handler.handleError(res)(err); }
        if (num === 0) { return res.send(404).end(); }
        res.status(200).json({ success: true });
    });
}

module.exports.destroyComments = function (req, res) {
    Product.update({ _id: req.params.id }, { $pull: { comments: { _id: req.params.commentId, 'user': req.user._id } } }, function (err, num) {
        if (err) { return Handler.handleError(res)(err); }
        if (num === 0) { return res.send(404).end(); }
        res.status(200).json({ success: true });
    });
}

module.exports.updateComments = function (req, res) {
    Product.update({ _id: req.params.id, 'comments._id': req.params.commentId }, { 'comments.$.content': req.body.content, 'comments.$.user': req.user.id }, function (err, num) {
        if (err) { return Handler.handleError(res)(err); }
        if (num === 0) { return res.send(404).end(); }
        res.status(200).json({ success: true });
    });
}

/* answersComments APIs */
module.exports.createCommentAnswer = function (req, res) {
    req.body.user = req.user.id;
    Product.update({ _id: req.params.id, 'comments._id': req.params.commentId }, { $push: { 'comments.$.answers': req.body } }, function (err, num) {
        if (err) { return Handler.handleError(res)(err); }
        if (num === 0) { return res.send(404).end(); }
        res.status(200).json({ success: true });
    })
}
module.exports.destroyCommentAnswer = function (req, res) {
    Product.update({ _id: req.params.id, 'comments._id': req.params.commentId }, { $pull: { 'comments.$.answers': { _id: req.params.answerId, 'user': req.user._id } } }, function (err, num) {
        if (err) { return Handler.handleError(res)(err); }
        if (num === 0) { return res.send(404).end(); }
        res.status(200).json({ success: true });
    });
}
module.exports.updateCommentAnswer = function (req, res) {
    Product.findById(req.params.id).exec(function (err, product) {
        if (err) { return Handler.handleError(res)(err); }
        if (!product) { return res.send(404).end(); }
        var found = false;
        for (var i = 0; i < product.comments.length; i++) {
            if (question.comments[i]._id.toString() === req.params.commentId) {
                found = true;
                var conditions = {};
                conditions._id = req.params.id;
                conditions['comments.' + i + '.answers._id'] = req.params.answerId;
                conditions['comments.' + i + '.answers.user'] = req.user._id;
                var doc = {};
                doc['comments.' + i + '.answers.$.content'] = req.body.content;
                /*jshint -W083 */
                Product.update(conditions, doc, function (err, num) {
                    if (err) { return Handler.handleError(res)(err); }
                    if (num === 0) { return res.send(404).end(); }
                    res.status(200).json({ success: true });
                    return;
                });
            }
        }
        if (!found) {
            return res.send(404).end();
        }
    });
}