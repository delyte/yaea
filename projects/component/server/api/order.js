var Order = require('../model/order');
var Handler = require('./handler');
var Promise = require('bluebird');
var _ = require('lodash');
var orderStatus = require('../model/codetable').Order.Status;

require('../model/product');
require('../model/recipient');
require('../model/user');


// Gets a single list from the DB
module.exports.index = function (req, res) {
    var pageindex = req.query.pageindex,
        skipCount = (pageindex - 1) * 10;
    var query = req.query.query && JSON.parse(req.query.query);
    Promise.all([
        Order.find(query).sort({ createdAt: -1 }).skip(skipCount).limit(10).populate('user','name').execAsync(),
        Order.countAsync(query)
    ])
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}


function showOrder(entity) {
    return Order.findById(entity._id)
        .populate('recipient', 'name phone address')
        .populate('orders.product', 'name price title cover').execAsync()
}


module.exports.queryCatgory = function (req, res) {
    var userid = req.user.id;
    var status = req.query.status;
    Order.findAsync({ 'user': userid, 'status': status })
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.catgoryCount = function (req, res) {
    var userid = req.user.id;
    var query = req.query.query && JSON.parse(req.query.query);
    Order.aggregateAsync([
        { $match: { 'user': userid } },
        { $group: { _id: "$status", count: { $sum: 1 } } }
    ])
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));;
}

module.exports.show = function (req, res) {
    Order.findById(req.params.id)
        .populate('recipient', 'name phone address')
        .populate('orders.product', 'name price title cover').execAsync()
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.create = function (req, res) {
    var userid = req.user.id;
    req.body.user = userid;
    Order.createAsync(req.body)
        .then(showOrder)
        .then(Handler.respondWithResult(res, 201))
        .catch(Handler.handleError(res));
}

module.exports.next = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    var userid = req.user.id;
    req.body.user = userid;
    req.body.status = nextStatus(req.body.status);
    Order.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.saveUpdates(req.body))
        .then(showOrder)
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

function nextStatus(status) {
    var index = _.indexOf(orderStatus, status);
    if (index < 0) {
        return orderStatus[2];
    }
    if (index == orderStatus.length - 1) {
        return orderStatus[index];
    }
    else {
        return orderStatus[index + 1];
    }
}

module.exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    var userid = req.user.id;
    req.body.user = userid;
    Order.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.saveUpdates(req.body))
        .then(Handler.respondWithResult(res))
        .catch(Handler.handleError(res));
}

module.exports.destroy = function (req, res) {
    Order.findByIdAsync(req.params.id)
        .then(Handler.handleEntityNotFound(res))
        .then(Handler.removeEntity(res))
        .catch(Handler.handleError(res));
}