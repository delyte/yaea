'use strict';

var Promise = require('bluebird');


var config = require('../../config');

var TopClient = require('./sdk/topClient').TopClient;


module.exports.send = function (signName, templateCode, number, param) {


    return new Promise(function (resolve, reject) {
        var client = new TopClient({
            'appkey': config.external.alidayu.Key,
            'appsecret': config.external.alidayu.Secret,
            'REST_URL': config.external.alidayu.REST_URL
        });

        client.execute('alibaba.aliqin.fc.sms.num.send', {
            'sms_type': 'normal',
            'sms_free_sign_name': signName,
            'sms_param': param,
            'rec_num': number,
            'sms_template_code': templateCode
        }, function (error, response) {
            if (!error) {
                resolve(response)
            }
            else {
                reject(error);
            }
        })
    });

};



module.exports.query = function (number, date, page, size) {


    return new Promise(function (resolve, reject) {
        var client = new TopClient({
            'appkey': config.external.alidayu.Key,
            'appsecret': config.external.alidayu.Secret,
            'REST_URL': config.external.alidayu.REST_URL
        });

        client.execute('alibaba.aliqin.fc.sms.num.query', {
            'rec_num': number,
            'query_date': date,
            'current_page': page,
            'page_size': size
        }, function (error, response) {
            if (!error) console.log(response);
            else console.log(error);
        });
    });

};





