var Weixin = require('../../model/weixin');

var Key = 'access_token';

var urllib = require('urllib');

var Promise = require('bluebird');

var config = require('../../config');

function tokenIsValid() {

    return new Promise(function (resolve, reject) {
        Weixin.findOneAsync({ key: Key }).then(function (data) {
            if (data && data.expiredTime > new Date()) {
                resolve({
                    valid: true,
                    token: data.content
                });

            }
            else {
                resolve({
                    valid: false,
                    token: null
                });
            }
        }).catch(function (err) {
            if (err) {
                reject(err);
            }
        })
    });

}

function processToken(err, data) {

    return new Promise(function (resolve, reject) {
        if (err) {
            reject(err);
        }
        else if (data) {
            var dataJson = JSON.parse(data.toString());
            Weixin.findOneAsync({ key: Key }).then(function (doc) {
                if (doc) {
                    doc.content = dataJson && (dataJson.access_token ? dataJson.access_token : doc.content);
                    doc.expiredTime = dataJson && (dataJson.expires_in ? dataJson.expires_in : doc.expiredTime);
                }
                else {
                    doc = new Weixin({
                        key: Key,
                        content: dataJson && dataJson.access_token,
                        expiredTime: dataJson && dataJson.expires_in
                    });
                }
                doc.save();
                resolve(doc.content);
            }).catch(function (err) {
                reject(err);
            })

        }

    });

};


function getAccessToken(data) {
    return new Promise(function (resolve, reject) {
        if (data.valid) {
            resolve(data.token);
            return;
        }
        urllib.request('https://api.weixin.qq.com/cgi-bin/token', {
            method: 'GET',
            data: {
                'grant_type': 'client_credential',
                'appid': config.external.weixin.appid,
                'secret': config.external.weixin.secret
            }
        }, function (err, data) {
            processToken(err, data).then(resolve, reject);
        });
    });
}



module.exports.get = function () {
    return tokenIsValid().then(getAccessToken)
}