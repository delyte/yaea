var Promise = require('bluebird');
var weixinConfig = require('../../config').external.weixin;
var OAuth = require('wechat-oauth'),
    oAuth = new OAuth(weixinConfig.appid, weixinConfig.secret);


module.exports.getAuthorizeURL = function (redirect, state, scope) {
    return oAuth.getAuthorizeURL(redirect, state, scope);
}

module.exports.processAuth = function (code, state) {
    return new Promise(function (resolve, reject) {
        var authInfo = {};
        try {
            getAuthAccessToken(code).then(getAuthWeixinUser).then(function (data) {
                authInfo.openid = data.openid;
                authInfo.state = state;
                if (data.weixinUser) {
                    authInfo.userInfo = data.weixinUser;
                }
                resolve(authInfo);
            }, reject)

        } catch (error) {
            reject(error);
        }

    });
}

function getAuthAccessToken(code) {
    return new Promise(function (resolve, reject) {
        oAuth.getAccessToken(code, function (err, result) {
            if (err) {
                reject(err);
            }
            else if (result) {
                resolve(result.data.openid);
            }

        });

    });
}

function getAuthWeixinUser(data) {
    return new Promise(function (resolve, reject) {
        if (data.scope != 'snsapi_userinfo') {
            resolve(data);
            return;
        }
        oAuth.getUser(data.openid, function (err, result) {
            if (err) {
                reject(err);
            }
            else if (result) {
                data.weixinUser = userinfo;
                resolve(data);
            }
        });
    });

}