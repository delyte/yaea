var Weixin = require('../../model/weixin');

var util = require('util');

var KeyFormat = '%s_ticket';

var urllib = require('urllib');

var Promise = require('bluebird');

var config = require('../../config');

var token = require('./token');

function ticketIsValid(ticketType) {

    return new Promise(function (resolve, reject) {
        var Key = util.format(KeyFormat, ticketType);
        Weixin.findOneAsync({ key: Key }).then(function (data) {
            if (data && data.expiredTime > new Date()) {
                resolve({
                    valid: true,
                    ticket: data.content
                });

            }
            else {
                resolve({
                    ticketType: ticketType,
                    valid: false,
                    ticket: null
                });
            }
        }).catch(reject)
    });

}

function processTicket(err, data, ticketType) {

    return new Promise(function (resolve, reject) {
        if (err) {
            reject(err);
        }
        else if (data) {
            var dataJson = JSON.parse(data.toString());
            var Key = util.format(KeyFormat, ticketType);
            Weixin.findOneAsync({ key: Key }).then(function (doc) {
                if (doc) {
                    doc.content = dataJson && (dataJson.ticket ? dataJson.ticket : doc.content);
                    doc.expiredTime = dataJson && (dataJson.expires_in ? dataJson.expires_in : doc.expiredTime);
                }
                else {
                    doc = new Weixin({
                        key: Key,
                        content: dataJson && dataJson.ticket,
                        expiredTime: dataJson && dataJson.expires_in
                    });
                }
                doc.save();
                resolve(doc.content);
            }).catch(reject)

        }

    });

};


function getTicket(info) {
    return new Promise(function (resolve, reject) {
        if (info.valid) {
            resolve(info.token);
            return;
        }

        token.get().then(function (token) {

            urllib.request('https://api.weixin.qq.com/cgi-bin/ticket/getticket', {
                method: 'GET',
                data: {
                    'access_token': token,
                    'type': info.ticketType
                }
            }, function (err, data) {
                processTicket(err, data, info.ticketType).then(resolve, reject);
            });
        });
    });
}



module.exports.get = function (ticketType) {
    return ticketIsValid(ticketType).then(getTicket)
}