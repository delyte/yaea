var urllib = require('urllib');
var Promise = require('bluebird');
var token = require('./token');


exports.create = function (menuJson) {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/cgi-bin/menu/create?access_token=' + token,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: menuJson,
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.get = function () {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/cgi-bin/menu/get',
                {
                    method: 'GET',
                    data: {
                        'access_token': token
                    }
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.delete = function () {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/cgi-bin/menu/delete',
                {
                    method: 'GET',
                    data: {
                        'access_token': token
                    }
                }).then(resolve, reject);
        }).catch(reject);
    });
}



