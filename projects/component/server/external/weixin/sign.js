
var raw = function (args) {
    var keys = Object.keys(args);
    keys = keys.sort()
    var newArgs = {};
    keys.forEach(function (key) {
        newArgs[key.toLowerCase()] = args[key];
    });

    var string = '';
    for (var k in newArgs) {
        string += '&' + k + '=' + newArgs[k];
    }
    string = string.substr(1);
    return string;
};

var rawPayConfig = function (args) {
    var keys = Object.keys(args);
    keys = keys.sort()
    var newArgs = {};
    keys.forEach(function (key) {
        newArgs[key] = args[key];
    });

    var string = '';
    for (var k in newArgs) {
        string += '&' + k + '=' + newArgs[k];
    }
    string = string.substr(1);
    return string;
};

var crypto = require('crypto');

module.exports.paySign = function (json, payKey) {
    var rawString = raw(json) + '&key=' + payKey;
    var sign = crypto.createHash('md5').update(rawString, 'utf8').digest("hex").toUpperCase();
    return sign;
}

module.exports.payConfigSign = function (json, payKey) {
    var rawString = rawPayConfig(json) + '&key=' + payKey;
    var sign = crypto.createHash('md5').update(rawString, 'utf8').digest("hex").toUpperCase();
    return sign;
}

module.exports.jsApiSign = function (json) {
    var rawString = raw(json);
    var signature = crypto.createHash('sha1').update(rawString, 'utf8').digest("hex");
    return signature;
}

