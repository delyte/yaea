var urllib = require('urllib');
var Promise = require('bluebird');
var token = require('./token');


exports.addCustomService = function (customServiceJson) {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/customservice/kfaccount/add?access_token=' + token,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: menuJson,
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.updateCustomService = function (customServiceJson) {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/customservice/kfaccount/update?access_token=' + token,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: menuJson,
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.deleteCustomService = function (customServiceJson) {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/customservice/kfaccount/del?access_token=' + token,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: menuJson,
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.getCustomService = function () {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/cgi-bin/customservice/getkflist',
                {
                    method: 'GET',
                    data: {
                        'access_token': token
                    }
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.sendCustomServiceMessage = function (messageInfo) {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=' + token,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: messageInfo,
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.getTemplateList = function () {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/cgi-bin/template/get_all_private_template',
                {
                    method: 'GET',
                    data: {
                        'access_token': token
                    }
                }).then(resolve, reject);
        }).catch(reject);
    });
}


exports.sendTemplateMessage = function (messageInfo) {
    return new Promise(function (resolve, reject) {
        token.get().then(function (token) {
            urllib.request('https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=' + token,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: messageInfo,
                }).then(resolve, reject);
        }).catch(reject);
    });
}