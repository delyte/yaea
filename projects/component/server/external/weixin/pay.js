var sign = require('./sign');
var utility = require('../../api/utility');
var Promise = require('bluebird');
var weixinConfig = require('../../config').external.weixin;
var xml2js = require('xml2js');
var urllib = require('urllib');

function prepareUnifiedOrderData(description, orderNumber, totalFee, openId) {
    return new Promise(function (resolve, reject) {
        var postData = {
            'appid': weixinConfig.appid,
            'mch_id': weixinConfig.payid,
            'nonce_str': utility.generateNonceString(32),
            'body': description,
            'out_trade_no': orderNumber,
            'total_fee': totalFee,
            'spbill_create_ip': utility.getIPAdress(),
            'notify_url': weixinConfig.notifyUrl,
            'trade_type': 'JSAPI',
            'openid': openId
        }

        var orderSign = sign.paySign(postData, weixinConfig.paykey);
        postData.sign = orderSign;
        resolve(postData);
    });
}



var unifiedOrder = function (unifiedOrderData) {
    return new Promise(function (resolve, reject) {
        var builder = new xml2js.Builder()
        var xml = builder.buildObject(unifiedOrderData);
        urllib.request('https://api.mch.weixin.qq.com/pay/unifiedorder',
            {
                method: 'POST',
                data: xml,
            }).then(resolve, reject);
    });
}

var processOrderResult = function (xmlData) {
    return new Promise(function (resolve, reject) {
        var parser = new xml2js.Parser({ trim: true, explicitArray: false, explicitRoot: false });
        parser.parseString(xmlData.data.toString(), function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                if (result.return_code == 'SUCCESS' && result.result_code == 'SUCCESS') {
                    resolve(result);
                }
                else {
                    reject(result);
                }
            }
        });
    });
};

var generatePayConfig = function (req) {
    return function (data) {
        return new Promise(function (resolve, reject) {
            var perpay = 'prepay_id=' + data.prepay_id,
                timestamp = (req.session.weixinJsSdkConfig && req.session.weixinJsSdkConfig.timestamp) ? req.session.weixinJsSdkConfig.timestamp : utility.createTimestamp(),
                nonceStr = (req.session.weixinJsSdkConfig && req.session.weixinJsSdkConfig.nonceStr) ? req.session.weixinJsSdkConfig.nonceStr : utility.generateNonceString(32),
                initConfig = {
                    'appId': weixinConfig.appid,
                    'timeStamp': timestamp,
                    'nonceStr': nonceStr,
                    'package': perpay,
                    'signType': 'MD5'
                }
            var paySign = sign.payConfigSign(initConfig, weixinConfig.paykey);
            var payConfig = {
                'appId': initConfig.appId,
                'timeStamp': initConfig.timeStamp,
                'nonceStr': initConfig.nonceStr,
                'package': initConfig.package,
                'signType': initConfig.signType,
                'paySign': paySign
            }
            console.log(payConfig);
            resolve(payConfig);
        });
    }

};

module.exports.payConfig = function (description, orderNumber, totalFee, openId, req) {
    return prepareUnifiedOrderData(description, orderNumber, totalFee, openId)
        .then(unifiedOrder)
        .then(processOrderResult)
        .then(generatePayConfig(req));
}


