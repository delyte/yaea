var tickit = require('./tickit');
var sign = require('./sign');
var utility = require('../../api/utility');
var Promise = require('bluebird');
var weixinConfig = require('../../config').external.weixin;

module.exports.config = function (url) {
    return new Promise(function (resolve, reject) {
        tickit.get('jsapi').then(function (jsapitickit) {
            var ret = {
                jsapi_ticket: jsapitickit,
                nonceStr: utility.generateNonceString(32),
                timestamp: utility.createTimestamp(),
                url: url
            };
            var apisign = sign.jsApiSign(ret);
            var config = {
                debug: process.env.NODE_ENV != 'production',
                appId: weixinConfig.appid,
                timestamp: ret.timestamp,
                nonceStr: ret.nonceStr,
                signature: apisign,
                jsApiList: weixinConfig.jsApiList
            }
            resolve(config);
        }).catch(reject)
    });
}
