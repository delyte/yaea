'use strict';



// All configurations will extend these options
// ============================================
var all = {

    user_jwt: {
        secrets: 'yaea-user-secret'
    },
    external: {
        alidayu: {
            Key: '23359698',
            Secret: 'd85172f50a4ff307cd38432c85a31476',
            REST_URL: 'http://gw.api.taobao.com/router/rest'
        },
        weixin: {
            appid: 'wx4fd644b91928647a',
            secret: 'c316361e6504b301d52856c77126a8cb',
            payid: '1280179201',
            paykey: '19d5e2fb55bf4b7a8b3ea685823d6077',
            notifyUrl: 'http://bestbangbang.ittun.com/api/weixin/paynotify',
            jsApiList: [
                'onMenuShareTimeline',
                'onMenuShareAppMessage',
                'hideOptionMenu',
                'showOptionMenu',
                'hideMenuItems',
                'showMenuItems',
                'getLocation',
                'scanQRCode',
                'chooseWXPay'
            ]
        }
    },
    productName: '测试商城'
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = all;
